package cc.bruce.seagull.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.model.Content;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/4.
 * Desc:高级搜索页面
 */
public class SearchActivity extends BaseActivity {

    /** 头部*/
    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 搜索的内容*/
    @ViewInject(id = R.id.etSearchContent)private EditText etSearchContent;
    /** 搜索的种类显示*/
    @ViewInject(id = R.id.tvSearchType)private TextView tvSearchType;
    /** 选择的种类按钮*/
    @ViewInject(id = R.id.llSearchType)private LinearLayout llSearchType;
    /** popWindow*/
    private PopupWindow popupWindow;
    /** 需要请求搜索的种类*/
    private String searchType = "keyword";private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;
    private String categrotyName;

    @Override
    protected int getContentView() {
        return R.layout.activity_search;
    }

    @Override
    protected void initView() {
        if(getIntent() != null && getIntent().getExtras() != null) {
            categrotyName = getIntent().getExtras().getString(Constants.INTENT_CATE_NAME);
        }
        if(!StringUtils.isBlank(categrotyName)) {
            topBar.settitleViewText("分类搜索");
        } else {
            topBar.settitleViewText(getResources().getString(R.string.title_search));
        }
        topBar.setText(TopBar.RIGHT_BUTTON, getResources().getString(R.string.text_search));
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        topBar.setRightButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                //跳转至搜索结果
                AjaxParams ajaxParams = new AjaxParams();
                if (!StringUtils.isBlank(categrotyName)) {
                    ajaxParams.put("cat", categrotyName);
                }
                ajaxParams.put(searchType, etSearchContent.getText().toString());
                Upload(ajaxParams, API.server + API.APISEARCH, SUCCESS);
            }
        });
    }

    @Override
    protected void initData() {
        finalHttp = new FinalHttp();

        loadingDialog = new MyLoadingDialog(mContext);
        llSearchType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                } else {
                    showPupUpWindow();
                }
            }
        });
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        JSONArray data = object.optJSONArray("data");
                        Intent intent = new Intent(mContext, SearchResultActivity.class);
                        if(data != null && data.length() > 0) {
                            List<Content> contentList = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                Content content = new Content().parse(data.optJSONObject(i));
                                contentList.add(content);
                            }
                            intent.putExtra("ResultList", (Serializable) contentList);
                        } else {
                            intent.putExtra("ResultList", (Serializable) new ArrayList<Content>());
                        }
                        startActivity(intent);
                    }
                    break;
            }
        }
    };

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_request_failure),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }

    /**
     * 显示pup window对话框
     */
    private void showPupUpWindow() {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        if (popupWindow == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.layout_popwidow, null);
            popupWindow = new PopupWindow(view, width / 3, RelativeLayout.LayoutParams.WRAP_CONTENT);
            setPopClickListener(view);
        }
        // 使其聚集
        popupWindow.setFocusable(true);
        // 设置允许在外点击消失
        popupWindow.setOutsideTouchable(true);
        // 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 显示的位置为:屏幕的宽度的一半-PopupWindow的高度的一半
        // int xPos = parent.getWidth();
        // System.out.println("xPos:"+xPos);
        // System.out.println("width / 6:"+width / 6);
        // System.out.println("parent.getWidth() / 5:"+parent.getWidth() / 5);
        popupWindow.showAsDropDown(llSearchType, 5, 20);
    }

    /** 设置POP内的事件监听等*/
    private void setPopClickListener(View view) {
        TextView tvPopKeyword = (TextView) view.findViewById(R.id.tvPopKeyword);
        TextView tvPopCat = (TextView) view.findViewById(R.id.tvPopCat);
        TextView tvPopProof = (TextView) view.findViewById(R.id.tvPopProof);
        TextView tvPopPort = (TextView) view.findViewById(R.id.tvPopPort);
        if(!StringUtils.isBlank(categrotyName)) {
            tvPopCat.setVisibility(View.GONE);
        } else {
            tvPopCat.setVisibility(View.VISIBLE);
        }
        tvPopKeyword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseType("keyword");
            }
        });
        tvPopCat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseType("cat");
            }
        });
        tvPopProof.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseType("proof");
            }
        });
        tvPopPort.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseType("port");
            }
        });
    }

    /** 选择类别后执行操作*/
    private void chooseType(String searchType) {
        this.searchType = searchType;
        if(searchType.equals("keyword")) {
            tvSearchType.setText("关键字");
        }
        if(searchType.equals("cat")) {
            tvSearchType.setText("类别");
        }
        if(searchType.equals("proof")) {
            tvSearchType.setText("文号");
        }
        if(searchType.equals("port")) {
            tvSearchType.setText("发文机关");
        }
        popupWindow.dismiss();
    }
}
