package cc.bruce.seagull.model;

import org.json.JSONObject;

/**
 * Created by Lemon on 2015/7/4.
 * Desc:菜单类别实体类
 */
public class Category extends BaseModel<Category> {

    /** 种类ID*/
    public String category_id;
    /** 种类名称*/
    public String category_name;
    /** 种类别名*/
    public String category_alias;
    /** 父类ID*/
    public String parent_id;

    @Override
    public Category parse(JSONObject jsonObject) {
        if(jsonObject != null && jsonObject.length() > 0){
            category_id = jsonObject.optString("category_id");
            category_name = jsonObject.optString("category_name");
            category_alias = jsonObject.optString("category_alias");
            parent_id = jsonObject.optString("parent_id");
            return this;
        }
        return null;
    }
}
