package cc.bruce.seagull.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONObject;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/5.
 * Desc:意见反馈页面
 */
public class FeedBackActivity extends BaseActivity {

    /** 头部*/
    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 反馈输入框*/
    @ViewInject(id = R.id.etFeedback)private EditText etFeedback;
    private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;

    @Override
    protected int getContentView() {
        return R.layout.activity_feedback;
    }

    @Override
    protected void initView() {
        topBar.settitleViewText(getResources().getString(R.string.title_feedback));
        topBar.setText(TopBar.RIGHT_BUTTON, getResources().getString(R.string.submit));
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        topBar.setRightButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                //提交反馈
                if(!StringUtils.isBlank(etFeedback.getText().toString())) {
                    AjaxParams ajaxParams = new AjaxParams();
                    ajaxParams.put("uid", PreferencesUtils.getString(mContext, Constants.KEY_UID));
                    ajaxParams.put("content", etFeedback.getText().toString());
                    Upload(ajaxParams, API.server + API.APIVIEW, SUCCESS);
                } else {
                    promptDialog = new PromptDialog(mContext, "提交失败", "意见反馈不能为空，请重新输入", "确定", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            promptDialog.dismiss();
                        }
                    });
                    promptDialog.show();
                }
            }
        });
    }

    @Override
    protected void initData() {
        loadingDialog = new MyLoadingDialog(mContext);
        finalHttp = new FinalHttp();
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String)msg.obj);
                        String message = object.optString("msg");
                        switch (Integer.parseInt(message)){
                            case 1:
                                //提交成功
                                promptDialog = new PromptDialog(mContext, "提交成功", "感谢您给我们提出宝贵的意见和建议！", "确定", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        promptDialog.dismiss();
                                        finish();
                                    }
                                });
                                promptDialog.show();
                                break;
                        }
                    }
                    break;
            }
        }
    };

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_submit_failure),
                        getResString(R.string.prompt_request_failure),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }
}
