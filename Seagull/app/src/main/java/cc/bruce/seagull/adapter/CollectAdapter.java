package cc.bruce.seagull.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cc.bruce.seagull.R;
import cc.bruce.seagull.activity.CollectListActivity;
import cc.bruce.seagull.model.Content;
import cc.bruce.seagull.view.PromptDialog;

/**
 * Created by Lemon on 2015/7/7.
 * Desc:收藏列表适配器
 */
public class CollectAdapter extends AbsAdapter<Content>{

    /** 是否为可编辑模式*/
    private boolean isEdit;
    private Context context;
    private PromptDialog promptDialog;

    public CollectAdapter(Activity context, int layout, boolean isEdit) {
        super(context, layout);
        this.context = context;
        this.isEdit = isEdit;
    }

    @Override
    public ViewHolder<Content> getHolder() {
        return new ContentViewHolder();
    }


    private class ContentViewHolder implements ViewHolder<Content> {

        /** 删除收藏按钮*/
        private ImageView ivDelete;
        /** 标题*/
        private TextView tvName;
        /** 发布机关*/
        private TextView tvPort;
        /** 发文时间*/
        private TextView tvPubTime;

        @Override
        public void initViews(View v, int position) {
            ivDelete = (ImageView) v.findViewById(R.id.ivDelete);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvPort = (TextView) v.findViewById(R.id.tvPort);
            tvPubTime = (TextView) v.findViewById(R.id.tvPubTime);
        }

        @Override
        public void updateData(final Content content, final int position) {
            if(isEdit) {
                ivDelete.setVisibility(View.VISIBLE);
            }else {
                ivDelete.setVisibility(View.GONE);
            }
            tvName.setText(content.name);
            tvPort.setText("摘要:" + content.summary);
            tvPubTime.setText("发文时间:" + content.pubtime);
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //删除收藏操作
                    System.out.println("coming");
                    promptDialog = new PromptDialog(context, context.getResources().getString(R.string.notice),
                            context.getResources().getString(R.string.prompt_delete_collect),
                            context.getResources().getString(R.string.confirm),
                            context.getResources().getString(R.string.cancel),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {//确定删除收藏操作
                                    ((CollectListActivity)context).doDeleteCollect(content.detailid, position);
                                    promptDialog.dismiss();
                                }
                            },  new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    promptDialog.dismiss();
                                }
                        });
                    promptDialog.show();
                }
            });
        }

        @Override
        public void doOthers(Content content, int position) {

        }
    }
}
