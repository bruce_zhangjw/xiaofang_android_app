package cc.bruce.seagull.view;

import android.app.Dialog;
import android.content.Context;
import android.media.Image;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cc.bruce.seagull.R;

/**
 * Created by Lemon on 2015/7/7.
 * Desc:详情菜单选项对话框
 */
public class DetailMenuDialog extends Dialog {

    /** 收藏图标*/
    private ImageView ivDetailCollect;
    /** 收藏按钮*/
    private LinearLayout llDetailCollect;
    /** 纠错图标*/
    private ImageView ivDetailCorrection;
    /** 纠错按钮*/
    private LinearLayout llDetailCorrection;
    /** 取消按钮*/
    private TextView tvDetailDialogCancel;
    /** 收藏监听*/
    private View.OnClickListener OnCollectListener;
    /** 纠错监听*/
    private View.OnClickListener OnCorrectionListener;
    /** 当前是否已收藏*/
    private boolean isCollect;

    public DetailMenuDialog(Context context) {
        super(context, R.style.MenuDialogTheme);
        init(context);
    }

    public DetailMenuDialog(Context context, boolean isCollect, View.OnClickListener OnCollectListener, View.OnClickListener OnCorrectionListener) {
        super(context, R.style.MenuDialogTheme);
        this.OnCollectListener = OnCollectListener;
        this.OnCorrectionListener = OnCorrectionListener;
        this.isCollect = isCollect;
        init(context);
    }

    private void init(Context context) {
        setContentView(R.layout.dialog_detail_menu);
        getWindow().setGravity(Gravity.BOTTOM);//设置dialog显示的位置
        getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        setCanceledOnTouchOutside(false);
//        setCancelable(false);

        tvDetailDialogCancel = (TextView) findViewById(R.id.tvDetailDialogCancel);
        tvDetailDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        ivDetailCollect = (ImageView) findViewById(R.id.ivDetailCollect);
        ivDetailCorrection = (ImageView) findViewById(R.id.ivDetailCorrection);
        llDetailCollect = (LinearLayout) findViewById(R.id.llDetailCollect);
        llDetailCollect.setOnClickListener(OnCollectListener);
        llDetailCorrection = (LinearLayout) findViewById(R.id.llDetailCorrection);
        llDetailCorrection.setOnClickListener(OnCorrectionListener);
        if(isCollect) {
            ivDetailCollect.setImageResource(R.mipmap.ic_star_sel);
        } else {
            ivDetailCollect.setImageResource(R.mipmap.ic_star_nor);
        }
    }
}
