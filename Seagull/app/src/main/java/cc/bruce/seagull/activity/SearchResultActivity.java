package cc.bruce.seagull.activity;

import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import net.tsz.afinal.annotation.view.ViewInject;

import java.util.ArrayList;
import java.util.List;

import cc.bruce.seagull.R;
import cc.bruce.seagull.adapter.SecondListAdapter;
import cc.bruce.seagull.model.Category;
import cc.bruce.seagull.model.Content;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/4.
 * Desc:搜索结果页面
 */
public class SearchResultActivity extends BaseActivity {

    /** 列表*/
    @ViewInject(id = R.id.listSearchResult)private ListView listSearchResult;
    /** 搜索结果数量显示*/
    @ViewInject(id = R.id.tvResultCount)private TextView tvResultCount;
    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 数据源*/
    private ArrayList<Content> contentList;
    /** 适配器*/
    private SecondListAdapter secondListAdapter;

    @Override
    protected int getContentView() {
        return R.layout.activity_search_result;
    }

    @Override
    protected void initView() {
        topBar.settitleViewText(getResources().getString(R.string.title_search_result));
        topBar.setHiddenButton(TopBar.RIGHT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void initData() {
        secondListAdapter = new SecondListAdapter(SearchResultActivity.this, R.layout.list_item_second);
        listSearchResult.setAdapter(secondListAdapter);

        contentList = (ArrayList<Content>) getIntent().getExtras().getSerializable("ResultList");
        if(contentList != null && contentList.size() > 0) {
            tvResultCount.setText("搜索结果共" + contentList.size() + "条");
            secondListAdapter.getDataList().clear();
            secondListAdapter.getDataList().addAll(contentList);
            secondListAdapter.notifyDataSetChanged();
        } else {
            tvResultCount.setText("搜索结果共0条");
        }
        listSearchResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra(Constants.INTENT_DETAILID, secondListAdapter.getDataList().get(position).detailid);
                intent.putExtra(Constants.INTENT_CATEGORY, secondListAdapter.getDataList().get(position).name);
                startActivity(intent);
            }
        });
    }
}
