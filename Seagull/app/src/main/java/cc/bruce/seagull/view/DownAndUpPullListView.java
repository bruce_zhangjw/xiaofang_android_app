package cc.bruce.seagull.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Scroller;

public class DownAndUpPullListView extends ViewGroup {
    private static final int DURATION_TIME = 300;
    private static final int TOUCH_SLOP = 25;
    private static final float DISTANCE_RATIO = 3.0f;
    private static final float DOWN_PULL_HEIGHT_RATIO = 0.125f;
    private static final float DOWN_PULL_MAX_HEIGHT_RATIO = 0.185f;

    private static final float UP_PULL_HEIGHT_RATIO = 0.125f;
    private static final float UP_PULL_MAX_HEIGHT_RATIO = 0.185f;

    private int mMaxDownPullDistance = 0;
    private int mDownPullHeight = 0;
    private int mDowPullDistance;
    private int mDownPullFirstDownY;
    private int mDownPullDownY;
    private int mDownPullState = DOWN_PULL_STATE_FINISH;
    private static final int DOWN_PULL_STATE_FINISH = 0;
    private static final int DOWN_PULL_STATE_PULL_TO_REFRESH = 1;
    private static final int DOWN_PULL_STATE_REFRESHING = 2;
    private static final int DOWN_PULL_STATE_RELEASE_TO_REFRESH = 3;
    private static final int DOWN_PULL_STATE_REFRESH_FINISH = 4;

    private int mMaxUpPullDistance = 0;
    private int mUpPullHeight = 0;
    private int mUpPullDistance;
    private int mUpPullFirstDownY;
    private int mUpPullDownY;
    private int mUpPullState = UP_PULL_STATE_FINISH;
    private static final int UP_PULL_STATE_FINISH = 0;
    private static final int UP_PULL_STATE_PULL_TO_REFRESH = 1;
    private static final int UP_PULL_STATE_REFRESHING = 2;
    private static final int UP_PULL_STATE_RELEASE_TO_REFRESH = 3;
    private static final int UP_PULL_STATE_REFRESH_FINISH = 4;


    private boolean mAbleDownPull = false;
    private boolean mIsDownPullMove = false;

    private boolean mAbleUpPull = false;
    private boolean mIsUpPullMove = false;

    private Paint mPaint = null;
    private boolean mIsInitView = false;
    private Rect mDownPullBackgroundRect;
    private Paint mDownPullBackgroundPaint;
    private Rect mRect;
    private Paint mTextPaint;
    private float mTextX, mTextY;

    private int mTouchSlop;
    private Scroller mDownPullScroller;

    private Paint mUpPullPaint = null;
    private Rect mUpPullBackgroundRect;
    private Paint mUpPullBackgroundPaint;
    private Rect mUpPullRect;
    private Paint mUpPullTextPaint;
    private float mUpPullTextX, mUpPullTextY;

    private Scroller mUpPullScroller;


    private DownPullRefreshListener mDownPullRefreshListener;
    private UpPullRefreshListener mUpPullRefreshListener;

    private ListView mListView;
    private boolean mDrawDownPullView = true;
    private boolean mDrawUpPullView = true;

    public DownAndUpPullListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public DownAndUpPullListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DownAndUpPullListView(Context context) {
        super(context, null);
    }

    private void init(Context context) {
        WindowManager windowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        float density = displayMetrics.density;
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();

        int wantTouchSlop = (int) (TOUCH_SLOP * density);
        mTouchSlop = Math.max(mTouchSlop, wantTouchSlop);
        mDownPullScroller = new Scroller(context, new LinearInterpolator());
        mUpPullScroller = new Scroller(context, new LinearInterpolator());

        mListView = new ListView(context);
        mListView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        addView(mListView, 0);
    }

    private void initView(int width, int height) {
        mIsInitView = true;
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.MAGENTA);
        mPaint.setStyle(Paint.Style.FILL);

        mUpPullPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mUpPullPaint.setColor(Color.MAGENTA);
        mUpPullPaint.setStyle(Paint.Style.FILL);

        mDownPullHeight = (int) (height * DOWN_PULL_HEIGHT_RATIO);
        mRect = new Rect(0, -mDownPullHeight, width, 0);
        mMaxDownPullDistance = (int) (height * DOWN_PULL_MAX_HEIGHT_RATIO);

        mUpPullHeight = (int) (height * UP_PULL_HEIGHT_RATIO);
        mUpPullRect = new Rect(0, height, width, height + mUpPullHeight);
        mMaxUpPullDistance = (int) (height * UP_PULL_MAX_HEIGHT_RATIO);

        mDownPullBackgroundRect = new Rect(0, -mMaxDownPullDistance, width, 0);
        mUpPullBackgroundRect = new Rect(0, height, width, height + mMaxUpPullDistance);

        mDownPullBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDownPullBackgroundPaint.setColor(Color.BLACK);
        mDownPullBackgroundPaint.setStyle(Paint.Style.FILL);

        mUpPullBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mUpPullBackgroundPaint.setColor(Color.BLACK);
        mUpPullBackgroundPaint.setStyle(Paint.Style.FILL);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextAlign(Align.CENTER);
        mTextPaint.setTextSize(40.0f);

        mUpPullTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mUpPullTextPaint.setColor(Color.WHITE);
        mUpPullTextPaint.setTextAlign(Align.CENTER);
        mUpPullTextPaint.setTextSize(40.0f);


        mTextX = width / 2;
        FontMetrics fm = mTextPaint.getFontMetrics();
        float textHeight = Math.abs(fm.ascent) + Math.abs(fm.descent);
        mTextY = -((mDownPullHeight - textHeight) / 2);


        mUpPullTextX = width / 2;
        FontMetrics fm1 = mUpPullTextPaint.getFontMetrics();
        float textHeight1 = Math.abs(fm1.ascent) + Math.abs(fm1.descent);
        mUpPullTextY = height + ((mUpPullHeight - textHeight1) / 2) + Math.abs(fm1.ascent);


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        mListView.measure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (mListView != null) {
            mListView.layout(0, 0, (r - l), (b - t));
        }
        if (changed && !mIsInitView) {
            initView((r - l), (b - t));
        }
    }

    @Override
    public void computeScroll() {
        if (mDownPullScroller.computeScrollOffset()) {
            if (!mIsDownPullMove) {
                mIsDownPullMove = true;
            }
            mDowPullDistance = mDownPullScroller.getCurrY();
            postInvalidate();
        } else {
            if (mIsDownPullMove) {
                if (mDownPullState == DOWN_PULL_STATE_PULL_TO_REFRESH
                        || mDownPullState == DOWN_PULL_STATE_REFRESH_FINISH) {
                    mDownPullState = DOWN_PULL_STATE_FINISH;
                }
                if (mDownPullState == DOWN_PULL_STATE_RELEASE_TO_REFRESH) {
                    mDownPullState = DOWN_PULL_STATE_REFRESHING;
                    if (mDownPullRefreshListener != null) {
                        mDownPullRefreshListener.onDownPullRefresh();
                    } else {
                        onDownPullRefreshFinish();
                    }
                }
                mIsDownPullMove = false;
            }
        }

        if (mUpPullScroller.computeScrollOffset()) {
            if (!mIsUpPullMove) {
                mIsUpPullMove = true;
            }
            mUpPullDistance = mUpPullScroller.getCurrY();
            postInvalidate();
        } else {
            if (mIsUpPullMove) {
                if (mUpPullState == UP_PULL_STATE_PULL_TO_REFRESH
                        || mUpPullState == UP_PULL_STATE_REFRESH_FINISH) {
                    mUpPullState = UP_PULL_STATE_FINISH;
                }
                if (mUpPullState == UP_PULL_STATE_RELEASE_TO_REFRESH) {
                    mUpPullState = UP_PULL_STATE_REFRESHING;
                    if (mUpPullRefreshListener != null) {
                        mUpPullRefreshListener.onUpPullRefresh();
                    } else {
                        onUpPullRefreshFinish();
                    }
                }
                mIsUpPullMove = false;
            }
        }

    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.translate(0, mDowPullDistance + mUpPullDistance);
        super.dispatchDraw(canvas);
        drawDownPullView(canvas);
        drawUpPullView(canvas);
        canvas.translate(0, -mDowPullDistance - mUpPullDistance);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (handleEvent(ev) || mDowPullDistance > 0) {
            return true;
        }
        if (handleEventUpPull(ev) || mUpPullDistance < 0) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    public ListView getActualListView() {
        return mListView;
    }

    public void setDrawDownPullView(boolean drawDownPullView) {
        mDrawDownPullView = drawDownPullView;
    }

    public void setDrawUpPullView(boolean drawUpPullView) {
        mDrawUpPullView = drawUpPullView;
    }

    private void drawDownPullView(Canvas canvas) {
        //绘制背景
        canvas.drawRect(mDownPullBackgroundRect, mDownPullBackgroundPaint);
        if (mDrawDownPullView) {
            //绘制下拉刷新的内容
            canvas.drawRect(mRect, mPaint);
            String text = null;
            switch (mDownPullState) {
                case DOWN_PULL_STATE_PULL_TO_REFRESH:
                    text = "下拉刷新";
                    break;
                case DOWN_PULL_STATE_RELEASE_TO_REFRESH:
                    text = "释放立即刷新";
                    break;
                case DOWN_PULL_STATE_REFRESHING:
                    text = "正在刷新...";
                    break;
            }
            if (!TextUtils.isEmpty(text)) {
                canvas.drawText(text, mTextX, mTextY, mTextPaint);
            }
        }
    }

    private void drawUpPullView(Canvas canvas) {
        canvas.drawRect(mUpPullBackgroundRect, mUpPullBackgroundPaint);
        if (mDrawUpPullView) {
            //绘制下拉刷新的内容
            canvas.drawRect(mUpPullRect, mUpPullPaint);
            String text = null;
            switch (mUpPullState) {
                case UP_PULL_STATE_PULL_TO_REFRESH:
                    text = "上拉加载";
                    break;
                case UP_PULL_STATE_RELEASE_TO_REFRESH:
                    text = "释放立即刷新";
                    break;
                case UP_PULL_STATE_REFRESHING:
                    text = "正在刷新...";
                    break;
            }
            if (!TextUtils.isEmpty(text)) {
                canvas.drawText(text, mUpPullTextX, mUpPullTextY, mUpPullTextPaint);
            }
        }
    }


    public void onDownPullRefreshFinish() {
        mDownPullState = DOWN_PULL_STATE_REFRESH_FINISH;
        int animationTime = (int) ((mDowPullDistance * 1.0f)
                / mMaxDownPullDistance * DURATION_TIME);
        mDownPullScroller.startScroll(0, mDowPullDistance, 0,
                -mDowPullDistance, animationTime);
        postInvalidate();

    }

    public void onUpPullRefreshFinish() {
        mUpPullState = UP_PULL_STATE_REFRESH_FINISH;
        int animationTime = (int) ((Math.abs(mUpPullDistance) * 1.0f)
                / mMaxUpPullDistance * DURATION_TIME);
        mUpPullScroller.startScroll(0, mUpPullDistance, 0,
                -mUpPullDistance, animationTime);
        postInvalidate();

    }

    private void checkIsAbleToDownPull(MotionEvent event) {
        if (mListView == null) {
            mAbleDownPull = false;
            return;
        }
        View firstChild = mListView.getChildAt(0);
        if (firstChild != null) {
            int firstVisiblePos = mListView.getFirstVisiblePosition();
            int top = firstChild.getTop();
            if (firstVisiblePos == 0 && top == 0) {
                //第一次要下拉的时候修改一下Y坐标的值
                if (!mAbleDownPull) {
                    mDownPullDownY = mDownPullFirstDownY = (int) event.getY();
                }
                mAbleDownPull = true;
            } else {
                if (mDowPullDistance != 0) {
                    mDowPullDistance = 0;
                }
                mAbleDownPull = false;
            }
        } else {
            mAbleDownPull = true;
        }
    }

    private void checkIsAbleToUpPull(MotionEvent event) {
        if (mListView == null) {
            mAbleUpPull = false;
            return;
        }
        ListAdapter adapter = mListView.getAdapter();
        if (adapter == null) {
            mAbleUpPull = false;
            return;
        }
        int childCount = mListView.getChildCount();
        int count = adapter.getCount();
        int height = mListView.getHeight();
        View lastChildView = mListView.getChildAt(childCount - 1);
        if (lastChildView != null) {
            int lastVisiblePos = mListView.getLastVisiblePosition();
            int bottom = lastChildView.getBottom();
            if (lastVisiblePos == (count - 1) && bottom == height) {
                //第一次要下拉的时候修改一下Y坐标的值
                if (!mAbleUpPull) {
                    mUpPullDownY = mUpPullFirstDownY = (int) event.getY();
                }
                mAbleUpPull = true;
            } else {
                if (mUpPullDistance != 0) {
                    mUpPullDistance = 0;
                }
                mAbleUpPull = false;
            }
        } else {
            mAbleUpPull = false;
        }
    }

    public void setDownPullRefreshListener(DownPullRefreshListener listener) {
        mDownPullRefreshListener = listener;
    }

    public void setUpPullRefreshListener(UpPullRefreshListener listener) {
        mUpPullRefreshListener = listener;
    }

    public interface DownPullRefreshListener {
        void onDownPullRefresh();
    }

    public interface UpPullRefreshListener {
        void onUpPullRefresh();
    }

    /**
     * 是否是自己消费事件
     *
     * @param ev
     * @return
     */
    private boolean handleEvent(MotionEvent ev) {
        checkIsAbleToDownPull(ev);
        if (mAbleDownPull) {
            int action = ev.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN: {
                    mDownPullDownY = mDownPullFirstDownY = (int) ev.getY();
                    break;
                }
                case MotionEvent.ACTION_MOVE: {
                    int y = (int) ev.getY();
                    int distance = y - mDownPullFirstDownY;
                    int moveDistance = y - mDownPullDownY;
                    mDownPullDownY = y;
                    if (moveDistance > 0) {
                        // 如果下拉刷新隐藏 并且移动的距离小于固定滑动距离
                        if (distance <= mTouchSlop && mDowPullDistance == 0) {
                            return false;
                        }
                        mDowPullDistance += ((moveDistance * 1.0f) / DISTANCE_RATIO);
                        mDowPullDistance = Math.min(Math.max(0, mDowPullDistance),
                                mMaxDownPullDistance);

                        if (mDownPullState != DOWN_PULL_STATE_REFRESHING) {
                            if (mDowPullDistance > mDownPullHeight) {
                                mDownPullState = DOWN_PULL_STATE_RELEASE_TO_REFRESH;
                            } else {
                                mDownPullState = DOWN_PULL_STATE_PULL_TO_REFRESH;
                            }
                        }
                        invalidate();
                        return true;
                    } else {
                        // 如果下拉刷新隐藏
                        if (mDowPullDistance == 0) {
                            return false;
                        } else {
                            mDowPullDistance += ((moveDistance * 1.0f) / DISTANCE_RATIO);
                            mDowPullDistance = Math.min(
                                    Math.max(0, mDowPullDistance),
                                    mMaxDownPullDistance);
                            if (mDownPullState != DOWN_PULL_STATE_REFRESHING) {
                                if (mDowPullDistance > mDownPullHeight) {
                                    mDownPullState = DOWN_PULL_STATE_RELEASE_TO_REFRESH;
                                } else {
                                    mDownPullState = DOWN_PULL_STATE_PULL_TO_REFRESH;
                                }
                            }
                            invalidate();
                            return true;
                        }
                    }
                }
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL: {
                    mDownPullFirstDownY = 0;
                    mDownPullDownY = 0;
                    if (mDownPullState == DOWN_PULL_STATE_RELEASE_TO_REFRESH) {
                        int animationTime = (int) (((mDowPullDistance - mDownPullHeight) * 1.0f)
                                / mMaxDownPullDistance * DURATION_TIME);
                        mDownPullScroller.startScroll(0, mDowPullDistance, 0,
                                mDownPullHeight - mDowPullDistance, animationTime);
                        invalidate();
                        return true;
                    } else if (mDownPullState == DOWN_PULL_STATE_PULL_TO_REFRESH
                            || mDownPullState == DOWN_PULL_STATE_REFRESHING) {
                        int animationTime = (int) ((mDowPullDistance * 1.0f)
                                / mMaxDownPullDistance * DURATION_TIME);
                        mDownPullScroller.startScroll(0, mDowPullDistance, 0,
                                -mDowPullDistance, animationTime);
                        invalidate();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean handleEventUpPull(MotionEvent ev) {
        checkIsAbleToUpPull(ev);
        if (mAbleUpPull) {
            int action = ev.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN: {
                    mUpPullDownY = mUpPullFirstDownY = (int) ev.getY();
                    break;
                }
                case MotionEvent.ACTION_MOVE: {
                    int y = (int) ev.getY();
                    int distance = y - mUpPullFirstDownY;
                    int moveDistance = y - mUpPullDownY;
                    mUpPullDownY = y;
                    if (moveDistance > 0) {
                        if (mUpPullDistance == 0) {
                            return false;
                        }
                        mUpPullDistance += ((moveDistance * 1.0f) / DISTANCE_RATIO);
                        mUpPullDistance = Math.max(Math.min(0, mUpPullDistance), -mMaxUpPullDistance);
                        if (mUpPullState != UP_PULL_STATE_REFRESHING) {
                            if (mUpPullDistance < -(mUpPullHeight)) {
                                mUpPullState = UP_PULL_STATE_RELEASE_TO_REFRESH;
                            } else {
                                mUpPullState = UP_PULL_STATE_PULL_TO_REFRESH;
                            }
                        }
                        invalidate();
                        return true;
                    } else {
                        // 上拉刷新
                        if (Math.abs(distance) <= mTouchSlop && mUpPullDistance == 0) {
                            return false;
                        }
                        mUpPullDistance += ((moveDistance * 1.0f) / DISTANCE_RATIO);
                        mUpPullDistance = Math.max(
                                Math.min(0, mUpPullDistance),
                                -mMaxUpPullDistance);
                        if (mUpPullState != UP_PULL_STATE_REFRESHING) {
                            if (mUpPullDistance < -(mUpPullHeight)) {
                                mUpPullState = UP_PULL_STATE_RELEASE_TO_REFRESH;
                            } else {
                                mUpPullState = UP_PULL_STATE_PULL_TO_REFRESH;
                            }
                        }
                        invalidate();
                        return true;
                    }
                }
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL: {
                    mUpPullFirstDownY = 0;
                    mUpPullDownY = 0;
                    if (mUpPullState == UP_PULL_STATE_RELEASE_TO_REFRESH) {
                        int animationTime = (int) (((Math.abs(mUpPullDistance) - mUpPullHeight) * 1.0f)
                                / mMaxUpPullDistance * DURATION_TIME);
                        mUpPullScroller.startScroll(0, mUpPullDistance, 0,
                                -mUpPullHeight - mUpPullDistance, animationTime);
                        invalidate();
                        return true;
                    } else if (mUpPullState == UP_PULL_STATE_PULL_TO_REFRESH
                            || mUpPullState == UP_PULL_STATE_REFRESHING) {
                        int animationTime = (int) ((Math.abs(mUpPullDistance) * 1.0f)
                                / mMaxUpPullDistance * DURATION_TIME);
                        mUpPullScroller.startScroll(0, mUpPullDistance, 0, -mUpPullDistance, animationTime);
                        invalidate();
                        return true;
                    }
                }
            }
        }
        return false;
    }


}
