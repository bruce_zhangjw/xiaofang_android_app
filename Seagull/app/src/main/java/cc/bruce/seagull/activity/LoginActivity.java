package cc.bruce.seagull.activity;

import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONException;
import org.json.JSONObject;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.util.CheckNetWorkUtil;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
/**
 *
 * @author: Created by Lemon on 2015年7月2日 下午9:09:17
 * @Description: 登录界面
 */
public class LoginActivity extends BaseActivity implements OnClickListener {

	private boolean isQuit = false;
	private Timer timer = new Timer();
	/** 忘记密码按钮*/
	@ViewInject(id = R.id.tvForgetPass)private TextView tvForgetPass;
	/** 用户名输入框*/
    @ViewInject(id = R.id.etUserName)private EditText etUserName;
	/** 密码输入框*/
    @ViewInject(id = R.id.etPassWord)private EditText etPassWord;
	/** 登录按钮*/
    @ViewInject(id = R.id.tvLogin)private TextView tvLogin;
    private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;

	@Override
	protected int getContentView() {
		return R.layout.activity_login;
	}

	@Override
	protected void initView() {
        loadingDialog = new MyLoadingDialog(mContext);
		tvForgetPass.setOnClickListener(this);
		tvLogin.setOnClickListener(this);
	}

	@Override
	protected void initData() {
        finalHttp = new FinalHttp();
        if(getIntent() != null && getIntent().getExtras() != null) {
            //修改密码返回重新登录
            if(getIntent().getExtras().getString(Constants.INTENT_RE_LOGIN).equals(Constants.INTENT_RE_LOGIN)) {
                etUserName.setText(PreferencesUtils.getString(mContext, Constants.KEY_ACCOUNT));
            }
        } else {
            //正常登录
            loadingDialog.show();
            if(checkNetwork()) {
                if(!StringUtils.isBlank(PreferencesUtils.getString(mContext, Constants.KEY_UID))) {
                    if(!StringUtils.isBlank(PreferencesUtils.getString(mContext, Constants.KEY_ACCOUNT))
                            && !StringUtils.isBlank(PreferencesUtils.getString(mContext, Constants.KEY_PASS))){
                        etUserName.setText(PreferencesUtils.getString(mContext, Constants.KEY_ACCOUNT));
                        etPassWord.setText(PreferencesUtils.getString(mContext, Constants.KEY_PASS));
                        AjaxParams ajaxParams = new AjaxParams();
                        ajaxParams.put("uname", PreferencesUtils.getString(mContext, Constants.KEY_ACCOUNT));
                        ajaxParams.put("pword", PreferencesUtils.getString(mContext, Constants.KEY_PASS));
                        Upload(ajaxParams, API.server + API.LOGIN, SUCCESS);
                    } else {
                        loadingDialog.dismiss();
//                        prompt("登录已过期，请重新登录");
                    }
                } else {
                    loadingDialog.dismiss();
//                    prompt("登录已过期，请重新登录");
                }
            }
        }
	}

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String)msg.obj);
                        String message = object.optString("msg");
                        JSONObject data = object.optJSONObject("data");
                        switch (Integer.parseInt(message)){
                            case 0:
                                //登陆成功
                                savePreferences(data.optString("uid"), data.optString("account"), data.optString("uname"));
                                //TODO 应在后台设置token是否过期标石，完成自动等率功能，暂且先保存
                                PreferencesUtils.putString(mContext, Constants.KEY_PASS, etPassWord.getText().toString());
                                startActivity(new Intent(mContext, MainActivity.class));
                                finish();
                                break;
                            case 1:
                                //用户不存在
                                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_login_failure),
                                        getResString(R.string.prompt_name_not_found),
                                        getResString(R.string.confirm), new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        promptDialog.dismiss();
                                    }
                                });
                                promptDialog.show();
                                break;
                            case 2:
                                //用户名或密码错误
                                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_login_failure),
                                        getResString(R.string.prompt_pass_wrong),
                                        getResString(R.string.confirm), new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        promptDialog.dismiss();
                                    }
                                });
                                promptDialog.show();
                                break;
                        }
                    }
                    break;
            }
            loadingDialog.dismiss();
        }
    };

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.tvForgetPass:
				//忘记密码
                promptDialog = new PromptDialog(mContext, "请联系管理员", "电话:651209",
                        getResString(R.string.prompt_need_supp),
                        getResString(R.string.cancel),
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                prompt("拨打电话");
                                Uri uri = Uri.parse("tel:" + "651209");
                                Intent intent = new Intent(Intent.ACTION_CALL, uri);
                                startActivity(intent);
                                promptDialog.dismiss();
                            }
                        }, new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                promptDialog.dismiss();
                            }
                    });
                promptDialog.show();
				break;
			case R.id.tvLogin:
				//登录按钮
                if(!StringUtils.isBlank(etUserName.getText().toString())) {
                    if(!StringUtils.isBlank(etPassWord.getText().toString())) {
                        AjaxParams ajaxParams = new AjaxParams();
                        ajaxParams.put("uname", etUserName.getText().toString());
                        ajaxParams.put("pword", etPassWord.getText().toString());
                        Upload(ajaxParams, API.server + API.LOGIN, SUCCESS);
                    }else {
                        prompt(getResources().getString(R.string.notice_password));
                    }
                }else {
                    prompt(getResources().getString(R.string.notice_username));
                }
				break;
			default:
				break;
		}
	}

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_not_work_),
                        getResString(R.string.confirm), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                loadingDialog.dismiss();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                loadingDialog.show();
                super.onStart();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return Key_Down(keyCode, event);
    }

    public boolean Key_Down(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (isQuit == false) {
				isQuit = true;
				Toast.makeText(getBaseContext(), "再按一次返回键退出程序",
						Toast.LENGTH_SHORT).show();
				TimerTask task = null;
				task = new TimerTask() {
					@Override
					public void run() {
						isQuit = false;
					}
				};
				timer.schedule(task, 2000);
			} else {
				finish();
				System.exit(0);
			}
		}
		return isQuit;
	}

	/** 检查网络*/
	private boolean checkNetwork() {
		if (!CheckNetWorkUtil.isNetwork(this)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setCancelable(false);
			builder.setMessage(getResources().getString(R.string.text_not_network));
			builder.setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));// 跳转至手机设置
				}
			});
			AlertDialog dialog = builder.create();
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.show();
			return false;
		}
		return true;
	}

    @Override
    protected void onDestroy() {
        if(loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
        super.onDestroy();
    }
}
