package cc.bruce.seagull.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.Content;

/**
 * Created by Lemon on 2015/7/7.
 * Desc:二级页面列表、搜索结果列表适配器
 */
public class SecondListAdapter extends AbsAdapter<Content>{

    public SecondListAdapter(Activity context, int layout) {
        super(context, layout);
    }

    @Override
    public ViewHolder<Content> getHolder() {
        return new ContentViewHolder();
    }


    private class ContentViewHolder implements ViewHolder<Content> {

        /** 标题*/
        private TextView tvName;
        /** 发布机关*/
        private TextView tvPort;
        /** 发文时间*/
        private TextView tvPubTime;

        @Override
        public void initViews(View v, int position) {
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvPort = (TextView) v.findViewById(R.id.tvPort);
            tvPubTime = (TextView) v.findViewById(R.id.tvPubTime);
        }

        @Override
        public void updateData(Content content, int position) {
            tvName.setText(content.name);
            tvPort.setText("摘要:" + content.summary);
            tvPubTime.setText("发文时间:" + content.pubtime);
        }

        @Override
        public void doOthers(Content content, int position) {

        }
    }
}
