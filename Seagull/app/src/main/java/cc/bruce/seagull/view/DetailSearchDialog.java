package cc.bruce.seagull.view;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cc.bruce.seagull.R;
import cc.bruce.seagull.activity.DetailActivity;
import cc.bruce.seagull.util.StringUtils;

/**
 * Created by Lemon on 2015/7/11.
 * Desc:详情关键字搜索对话框
 */
public class DetailSearchDialog extends Dialog {

    /** 清除按钮*/
    private ImageView ivSearchClear;
    /** 上一个按钮*/
    private ImageView ivSearchArrowLeft;
    /** 下一条按钮*/
    private ImageView ivSearchArrowRight;
    /** 搜索总条目显示*/
    private TextView tvSearchCount;
    /** 搜索条目当前索引*/
    private TextView tvSearchIndex;
    /** 输入关键字*/
    private EditText etSearchKeyWord;
    /** 清除监听*/
    private View.OnClickListener OnClearListener;
    /** 上一条监听*/
    private View.OnClickListener OnPreviousListener;
    /** 下一条监听*/
    private View.OnClickListener OnNextListener;

    public DetailSearchDialog(Context context) {
        super(context, R.style.MenuDialogTheme);
        init(context);
    }

    public DetailSearchDialog(Context context, View.OnClickListener OnClearListener,
                              View.OnClickListener OnPreviousListener,
                              View.OnClickListener OnNextListener) {
        super(context, R.style.MenuDialogTheme);
        this.OnClearListener = OnClearListener;
        this.OnPreviousListener = OnPreviousListener;
        this.OnNextListener = OnNextListener;
        init(context);
    }

    private void init(final Context context) {
        setContentView(R.layout.dialog_detail_search);
        getWindow().setGravity(Gravity.TOP);//设置dialog显示的位置
        getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        setCanceledOnTouchOutside(false);
//        setCancelable(false);

        ivSearchClear = (ImageView) findViewById(R.id.ivSearchClear);
        ivSearchArrowLeft = (ImageView) findViewById(R.id.ivSearchArrowLeft);
        ivSearchArrowRight = (ImageView) findViewById(R.id.ivSearchArrowRight);

        tvSearchCount = (TextView) findViewById(R.id.tvSearchCount);
        tvSearchIndex = (TextView) findViewById(R.id.tvSearchIndex);
        etSearchKeyWord = (EditText) findViewById(R.id.etSearchKeyWord);

        ivSearchClear.setOnClickListener(OnClearListener);
        ivSearchArrowLeft.setOnClickListener(OnPreviousListener);
        ivSearchArrowRight.setOnClickListener(OnNextListener);

        etSearchKeyWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StringUtils.isBlank(s.toString())) {
                    Log.e("DIALOG", "afterTextChanged->" + s.toString());
                    ((DetailActivity) context).doSearchKeyWord(s.toString());
                } else {
                    ((DetailActivity) context).doClear();
                }
            }
        });
    }

    /** 清除关键字*/
    public void clearKeyWord() {
        etSearchKeyWord.setText("");
        tvSearchCount.setVisibility(View.GONE);
        tvSearchIndex.setVisibility(View.GONE);
    }

//    /** 显示搜索到的结果*/
//    public void showSearchCount(int totalCount) {
//        if(totalCount == 0){
//            tvSearchCount.setVisibility(View.GONE);
//            tvSearchIndex.setVisibility(View.GONE);
//        } else {
//            tvSearchIndex.setText("1");
//            tvSearchCount.setText("/" + totalCount);
//            tvSearchIndex.setVisibility(View.VISIBLE);
//            tvSearchCount.setVisibility(View.VISIBLE);
//        }
//    }

    /** 显示搜索到的结果*/
    public void showSearchCount(int totalCount,int currentIndex) {
        tvSearchIndex.setText(currentIndex+"");
        tvSearchCount.setText("/" + totalCount);
        tvSearchIndex.setVisibility(View.VISIBLE);
        tvSearchCount.setVisibility(View.VISIBLE);
    }

    /** 改变索引位置*/
    public void changeIndex(int currentIndex) {
        tvSearchIndex.setText(currentIndex + "");
        tvSearchIndex.setVisibility(View.VISIBLE);
    }
}
