package cc.bruce.seagull.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import cc.bruce.seagull.R;

/**
 * Created by Lemon on 2015/7/8.
 * Desc:详情收藏后显示结果对话框
 */
public class CollectResultDialog extends Dialog {

    /** 取消按钮*/
    private TextView tvCollectResult;
    private String collectResult;

    public CollectResultDialog(Context context) {
        super(context, R.style.MenuDialogTheme);
        init(context);
    }

    public CollectResultDialog(Context context, String collectResult) {
        super(context, R.style.MenuDialogTheme);
        this.collectResult = collectResult;
        init(context);
    }

    private void init(Context context) {
        setContentView(R.layout.dialog_collect_result);
        getWindow().setGravity(Gravity.CENTER);//设置dialog显示的位置
        getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        tvCollectResult = (TextView) findViewById(R.id.tvCollectResult);
        tvCollectResult.setText(collectResult);
    }
}
