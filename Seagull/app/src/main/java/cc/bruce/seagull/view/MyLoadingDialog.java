package cc.bruce.seagull.view;


import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import cc.bruce.seagull.R;

/**
 * 
 * @author Administrator
 *@desc 加载时的对话框
 */
public class MyLoadingDialog extends Dialog{
	private TextView tv;
	
	public MyLoadingDialog(Context context) {
		
		super(context, R.style.MyDialogStyle);
		// TODO Auto-generated constructor stub
		 View  view=LayoutInflater.from(context).inflate(R.layout.layout_progress_dialog, null);
		 	tv=(TextView) view.findViewById(R.id.d_textview);
		 	setCanceledOnTouchOutside(false);
		 	setContentView(view);
		 	setCancelable(false);
//		 	getWindow().getAttributes().alpha=0.8f;
//		 	getWindow().setAttributes(getWindow().getAttributes());  

	}
	   public void setMsg(String msg){
		   tv.setText(msg);
   }
   public void setMsg(int msgId){
	   tv.setText(msgId);
   }

}	
