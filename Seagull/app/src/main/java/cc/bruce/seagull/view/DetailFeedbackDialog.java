package cc.bruce.seagull.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cc.bruce.seagull.R;

/**
 * Created by Lemon on 2015/7/8.
 * Desc:详情纠错对话框
 */
public class DetailFeedbackDialog extends Dialog {


    /** 纠错输入框*/
    private EditText etFeedbackContent;
    /** 取消按钮*/
    private TextView tvCancel;
    /** 发送按钮*/
    private TextView tvSend;
    /** 发送监听*/
    private View.OnClickListener onSendClickListener;

    public DetailFeedbackDialog(Context context) {
        super(context, R.style.MenuDialogTheme);
        init(context);
    }

    public DetailFeedbackDialog(Context context, View.OnClickListener onSendClickListener) {
        super(context, R.style.MenuDialogTheme);
        this.onSendClickListener = onSendClickListener;
        init(context);
    }

    private void init(Context context) {
        setContentView(R.layout.dialog_feedback);
        getWindow().setGravity(Gravity.TOP);//设置dialog显示的位置
        getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        etFeedbackContent = (EditText) findViewById(R.id.etFeedbackContent);
        tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvSend = (TextView) findViewById(R.id.tvSend);
        tvSend.setOnClickListener(onSendClickListener);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    /** 获取纠错内容*/
    public String getFeedbackContent() {
        return etFeedbackContent.getText().toString();
    }
}
