package cc.bruce.seagull.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cc.bruce.seagull.R;
import cc.bruce.seagull.adapter.CollectAdapter;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.model.Content;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.RefreshListView;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/7.
 * Desc:个人收藏列表
 */
public class CollectListActivity extends BaseActivity {

    /** 头部*/
    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 列表listView*/
    @ViewInject(id = R.id.listCollect)private RefreshListView listCollect;
    /** 删除收藏成功标石*/
    private static final int DELETE_COLLECT_SUCCESS = 200;
    /** 数据源*/
    private List<Content> contentList;
    /** 适配器*/
    private CollectAdapter collectAdapter;
    private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;
    /** 当前模式:false->关闭编辑模式；true->开启编辑模式*/
    private boolean isEdit = false;
    /** 当前请求删除的detailId*/
    private int currentPosition;
    /** 是否到最后一页*/
    boolean doNotOver = true;
    /** 是否已经提醒过一遍*/
    boolean isTip = false;
    private int page = Constants.START;
    private int pageSize = Constants.END;
    private int count = Constants.COUNT;

    @Override
    protected int getContentView() {
        return R.layout.activity_collect;
    }

    /** 请求封装参数*/
    private void AjaxParamsToUpload(int start, int end) {
        AjaxParams ajaxParams = new AjaxParams();
        ajaxParams.put("uid", PreferencesUtils.getString(mContext, Constants.KEY_UID));
        ajaxParams.put("start", start + "");
        ajaxParams.put("end", end + "");
        Upload(ajaxParams, API.server + API.APICOLLLIST, SUCCESS);
    }

    @Override
    protected void initView() {
        topBar.settitleViewText(getResources().getString(R.string.title_collect));
        topBar.setText(TopBar.RIGHT_BUTTON, getResources().getString(R.string.edit));
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        topBar.setRightButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                //编辑模式
                if (isEdit) {
                    //关闭编辑模式
                    isEdit = false;
                    topBar.setText(TopBar.RIGHT_BUTTON, getResources().getString(R.string.edit));
                } else {
                    //开启编辑模式
                    isEdit = true;
                    topBar.setText(TopBar.RIGHT_BUTTON, getResources().getString(R.string.finish));
                }
                collectAdapter = new CollectAdapter(CollectListActivity.this, R.layout.list_item_collect, isEdit);
                listCollect.setAdapter(collectAdapter);
                refreshList();
            }
        });
    }

    @Override
    protected void initData() {
        finalHttp = new FinalHttp();
        loadingDialog = new MyLoadingDialog(mContext);

        AjaxParamsToUpload(page, pageSize);

        listCollect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra(Constants.INTENT_DETAILID, collectAdapter.getDataList().get(position - 1).detailid);
                intent.putExtra(Constants.INTENT_CATEGORY, collectAdapter.getDataList().get(position - 1).name);
                startActivity(intent);
            }
        });

        listCollect.setOnLoadMoreListenter(new RefreshListView.OnLoadMoreListener() {

            public void onLoadMore() {
                Log.e(TAG, "setOnLoadMoreListenter");
                if (doNotOver) {
                    pageSize = pageSize + count;
                    AjaxParamsToUpload(page, pageSize);
                } else {
                    listCollect.onLoadMoreComplete();
                    if (!isTip) {
                        isTip = true;
                        prompt("已经到底了");
                    }
                }
            }
        });
        listCollect.setOnRefreshListener(new RefreshListView.OnRefreshListener() {

            public void onRefresh() {
                Log.e(TAG, "setOnRefreshListener");
                pageSize = Constants.END;
                AjaxParamsToUpload(page, pageSize);
            }
        });
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    loadingDialog.dismiss();
                    listCollect.onRefreshComplete();
                    listCollect.onLoadMoreComplete();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        JSONArray data = object.optJSONArray("data");
                        if(!StringUtils.isBlank(object.optString("listnum"))) {
                            int totalCount =  Integer.parseInt(object.optString("listnum"));
                            if(pageSize >= totalCount) {
                                //已经请求到最后的数据
                                doNotOver = false;
                            } else {
                                //还有一下页
                                doNotOver = true;
                            }
                        }
                        if(data != null && data.length() > 0) {
                            contentList = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                Content content = new Content().parse(data.optJSONObject(i));
                                contentList.add(content);
                            }
                            refreshList();
                        }
                    }
                    break;
                case DELETE_COLLECT_SUCCESS:
                    //删除收藏成功
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        String message = object.optString("msg");
                        if(message.equals("0")) {
                            prompt("操作失败，请重试");
                        }
                        if(message.equals("1")) {
                            prompt("已取消收藏");
                        }
                        contentList.remove(currentPosition);
                        refreshList();
                    }
                    break;
            }
        }
    };

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_not_work),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                listCollect.onRefreshComplete();
                listCollect.onLoadMoreComplete();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    listCollect.onRefreshComplete();
                    listCollect.onLoadMoreComplete();
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }

    /** 刷新列表*/
    private void refreshList() {
        if(contentList != null && contentList.size() > 0) {
            collectAdapter.getDataList().clear();
            collectAdapter.getDataList().addAll(contentList);
            collectAdapter.notifyDataSetChanged();
        }
    }

    /** 执行删除收藏操作*/
    public void doDeleteCollect(String detailId, int position) {
        AjaxParams ajaxParams = new AjaxParams();
        ajaxParams.put("uid", PreferencesUtils.getString(mContext, Constants.KEY_UID));
        ajaxParams.put("detailid", detailId);
        this.currentPosition = position;
        Upload(ajaxParams, API.server + API.APICOLLECT, DELETE_COLLECT_SUCCESS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        collectAdapter = new CollectAdapter(CollectListActivity.this, R.layout.list_item_collect, isEdit);
        listCollect.setAdapter(collectAdapter);
        refreshList();
    }
}
