package cc.bruce.seagull.activity;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/5.
 * Desc:修改用户名页面
 */
public class EditUserNameActivity extends BaseActivity {

    /** 头部*/
    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 用户名输入框*/
    @ViewInject(id = R.id.etNickName)private EditText etNickName;
    /** 青空按钮*/
    @ViewInject(id = R.id.ivClean)private ImageView ivClean;
    private FinalHttp finalHttp;
    /** 从本地缓存中取出当前用户名*/
    private String nickName;
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;

    @Override
    protected int getContentView() {
        return R.layout.activity_edit_username;
    }

    @Override
    protected void initView() {
        topBar.settitleViewText(getResources().getString(R.string.title_nickname));
        topBar.setText(TopBar.RIGHT_BUTTON, getResources().getString(R.string.save));
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        topBar.setRightButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                //修改用户名请求
                if(!StringUtils.isBlank(etNickName.getText().toString())) {
                    if(etNickName.equals(nickName)) {
                        //未修改,直接关闭页面，无需请求接口等
                        finish();
                    }else {
                        //有更改
                        AjaxParams ajaxParams = new AjaxParams();
                        ajaxParams.put("uid", PreferencesUtils.getString(mContext, Constants.KEY_UID));
                        ajaxParams.put("name", etNickName.getText().toString());
                        Upload(ajaxParams, API.server + API.APIUNAME, SUCCESS);
                    }
                }else {
                    prompt(getResString(R.string.text_name_not_empty));
                }
            }
        });
    }

    @Override
    protected void initData() {
        finalHttp = new FinalHttp();
        loadingDialog = new MyLoadingDialog(mContext);
        nickName = PreferencesUtils.getString(mContext, Constants.KEY_USERNAME);
        etNickName.setText(nickName);

        ivClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //清除
                etNickName.setText("");
            }
        });
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    if(msg.obj != null) {
                        PreferencesUtils.putString(mContext, Constants.KEY_USERNAME, etNickName.getText().toString());
                        prompt(getResString(R.string.text_edit_name_success));
                        finish();
                    }
                    break;
            }
            loadingDialog.dismiss();
        }
    };

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_not_work),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }
}
