package cc.bruce.seagull.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.model.Category;
import cc.bruce.seagull.util.ActivityStackManager;
import cc.bruce.seagull.util.AppInfoUtil;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PackageUtil;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.TopBar;
/**
 * Created by Lemon on 2015/7/4.
 * Desc:设置界面
 */
public class SettingActivity extends BaseActivity {

    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 显示版本号*/
    @ViewInject(id = R.id.tvVersionCode)private TextView tvVersionCode;
    /** 显示当前缓存*/
    @ViewInject(id = R.id.tvCache)private TextView tvCache;
    @ViewInject(id = R.id.tvQuit)private TextView tvQuit;
    private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;
    /** 完整的下载路径*/
    private String versionUrl;
    /** 获取最新版本成功*/
    private static final int GET_VERSION_SUCCESS = 200;
    /** 获取下载路径成功*/
    private static final int GET_VERSION_URL_SUCCESS = 300;

    @Override
    protected int getContentView() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView() {
        topBar.settitleViewText(getResources().getString(R.string.title_setting));
        topBar.setHiddenButton(TopBar.RIGHT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        finalHttp = new FinalHttp();
        loadingDialog = new MyLoadingDialog(mContext);
        tvVersionCode.setText(PackageUtil.getAppVersionName(mContext));

        AppInfoUtil.getPkgSize(mContext, handler, 1000);

        promptQuitDialog = new PromptDialog(mContext, "提示", "确定要退出当前账号？",
                "确定", "取消", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //退出
                promptQuitDialog.dismiss();
                PreferencesUtils.clear(mContext);
                ActivityStackManager.getActivityManager().finishAllExceptOne(SettingActivity.class);
                startActivity(new Intent(mContext, LoginActivity.class));
                finish();
            }
        },new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptQuitDialog.dismiss();
            }
        });
    }

    PromptDialog promptQuitDialog;

    @Override
    protected void initData() {
        findViewById(R.id.tvQuit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //退出
                promptQuitDialog.show();
            }
        });
        findViewById(R.id.llSettingUserName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //修改用户名
                startActivity(new Intent(mContext, EditUserNameActivity.class));
            }
        });
        findViewById(R.id.llSettingPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //修改密码
                startActivity(new Intent(mContext, EditPassWordActivity.class));
            }
        });
        findViewById(R.id.llSettingCollect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //个人收藏
                startActivity(new Intent(mContext, CollectListActivity.class));
            }
        });
        findViewById(R.id.llSettingFeedBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //意见反馈
                startActivity(new Intent(mContext, FeedBackActivity.class));
            }
        });
        findViewById(R.id.llSettingUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //版本检测
                Upload(new AjaxParams(), API.server + API.APIVERSION, GET_VERSION_SUCCESS);
            }
        });
        findViewById(R.id.llSettingCleanCache).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //清除缓存
                promptDialog = new PromptDialog(mContext, "提示", "确定要删除所有缓存?", "确定", "取消", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingDialog.show();
                        AppInfoUtil.cleanApplicationData(mContext, handler, 2000);
                        promptDialog.dismiss();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
            }
        });
        findViewById(R.id.llSettingAboutUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //关于我们
                startActivity(new Intent(mContext, AboutUsActivity.class));
            }
        });
        findViewById(R.id.llSettingBBS).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //交流区
                startActivity(new Intent(mContext, BBSActivity.class));
            }
        });
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case GET_VERSION_SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        if(object.optJSONObject("data") != null && object.optJSONObject("data").length() > 0) {
                            if(!StringUtils.isBlank(object.optJSONObject("data").optString("ver"))
                                    && !object.optJSONObject("data").optString("ver").equals("null")) {
                                String newVersion = object.optJSONObject("data").optString("ver");
                                String oldVersion = PackageUtil.getAppVersionName(mContext);
                                if(Double.parseDouble(newVersion) > Double.parseDouble(oldVersion)) {
                                    Log.e(TAG, "newVersion->" + newVersion + "| oldVersion->" + oldVersion);
                                    AjaxParams ajaxParam = new AjaxParams();
                                    ajaxParam.put("version", newVersion);
                                    Upload(ajaxParam, API.server + API.APIAPP, GET_VERSION_URL_SUCCESS);
                                } else {
                                    //提示用户是最新版本
                                    promptDialog = new PromptDialog(mContext, "提示", "已经是最新版本", "确定", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            promptDialog.dismiss();
                                        }
                                    });
                                    promptDialog.show();
                                }
                            } else {
                                //提示用户是最新版本
                                promptDialog = new PromptDialog(mContext, "提示", "已经是最新版本", "确定", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        promptDialog.dismiss();
                                    }
                                });
                                promptDialog.show();
                            }
                        }
                    }
                    break;
                case GET_VERSION_URL_SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        if(!StringUtils.isBlank(object.optString("download_url"))) {
                            versionUrl = API.server + object.optString("download_url");
                            Log.e(TAG, "下载APP路径->" + versionUrl);
                            promptDialog = new PromptDialog(mContext, "提示", "发现新版本", "马上更新", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //下载
                                    downloadApk(versionUrl);
                                    promptDialog.dismiss();
                                }
                            });
                            promptDialog.show();
                        }
                    }
                    break;
                case 1000:
                    if(msg.obj != null) {
                        String size = (String) msg.obj;
                        Log.e(TAG, "size->" + size);
                        tvCache.setText(size);
                    }
                    break;
                case 2000:
                    loadingDialog.dismiss();
                    tvCache.setText("0 KB");
                    break;
            }
        }
    };

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_request_failure),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }

    private void downloadApk(String url){
        final ProgressDialog downloadDialog = new ProgressDialog(mContext, R.style.LightDialog);
        downloadDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        downloadDialog.setTitle("正在下载");
        downloadDialog.setIndeterminate(false);
        downloadDialog.setCancelable(false);
        downloadDialog.setCanceledOnTouchOutside(false);
        downloadDialog.show();
        finalHttp.download(url, Constants.APK_TARGET, new AjaxCallBack<File>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                System.out.println("errorNo:" + errorNo + ",strMsg:" + strMsg);
                downloadDialog.dismiss();
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_request_failure),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
                int progress;
                if (current != count && current != 0) {
                    progress = (int) (current / (float) count * 100);
                } else {
                    progress = 100;
                }
                downloadDialog.setProgress(progress);
                Log.e(TAG, "download:" + progress + "%");
            }

            @Override
            public void onStart() {
                super.onStart();
                Log.e(TAG, "start download");
            }

            @Override
            public void onSuccess(File t) {
                super.onSuccess(t);
                Log.e(TAG, "download success");
                PackageUtil.installNormal(mContext, Constants.APK_TARGET);
                downloadDialog.dismiss();
            }
        });
    }
}
