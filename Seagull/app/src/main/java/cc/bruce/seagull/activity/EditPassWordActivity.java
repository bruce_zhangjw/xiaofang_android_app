package cc.bruce.seagull.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONObject;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/5.
 * Desc:修改用户名页面
 */
public class EditPassWordActivity extends BaseActivity {

    /** 头部*/
    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 用户名显示 你的账号名为：admin*/
    @ViewInject(id = R.id.tvNickName)private TextView tvNickName;
    /** 提交修改按钮*/
    @ViewInject(id = R.id.tvSubmit)private TextView tvSubmit;
    /** 旧密码输入框*/
    @ViewInject(id = R.id.etOldPass)private EditText etOldPass;
    /** 新密码输入框*/
    @ViewInject(id = R.id.etNewPass)private EditText etNewPass;
    /** 确认新密码输入框*/
    @ViewInject(id = R.id.etConfirmNewPass)private EditText etConfirmNewPass;
    private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;

    @Override
    protected int getContentView() {
        return R.layout.activity_edit_password;
    }

    @Override
    protected void initView() {
        topBar.settitleViewText(getResources().getString(R.string.title_edit_pass));
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void initData() {
        loadingDialog = new MyLoadingDialog(mContext);
        finalHttp = new FinalHttp();
        tvNickName.setText("您的账号名为:" + PreferencesUtils.getString(mContext, Constants.KEY_USERNAME));
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //提交修改
                if (verifyPass()) {
                    AjaxParams ajaxParams = new AjaxParams();
                    ajaxParams.put("uid", PreferencesUtils.getString(mContext, Constants.KEY_UID));
                    ajaxParams.put("account", PreferencesUtils.getString(mContext, Constants.KEY_ACCOUNT));
                    ajaxParams.put("oldpass", etOldPass.getText().toString());
                    ajaxParams.put("newpass", etNewPass.getText().toString());
                    Upload(ajaxParams, API.server + API.APIPASWORD, SUCCESS);
                }
            }
        });
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String)msg.obj);
                        String message = object.optString("msg");
                        switch (Integer.parseInt(message)){
                            case 0:
                                //原密码输入有误
                                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_edit_failure),
                                        getResString(R.string.prompt_old_pass_wrong), getResString(R.string.confirm), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        promptDialog.dismiss();
                                    }
                                });
                                promptDialog.show();
                                break;
                            case 1:
                                //修改成功
                                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_edit_success),
                                        getResString(R.string.prompt_edit_need_login), getResString(R.string.confirm), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        promptDialog.dismiss();
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.putExtra(Constants.INTENT_RE_LOGIN, Constants.INTENT_RE_LOGIN);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                promptDialog.show();
                                break;
                        }
                    }
                    break;
            }
        }
    };

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_edit_failure),
                        getResString(R.string.prompt_not_work),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }

    /** 校验密码*/
    private boolean verifyPass(){
        Boolean isTrue = true;
        String oldPass = etOldPass.getText().toString();
        String newPass = etNewPass.getText().toString();
        String confirmNewPass = etConfirmNewPass.getText().toString();
        if(!StringUtils.isBlank(oldPass) && !StringUtils.isBlank(newPass) && !StringUtils.isBlank(confirmNewPass)){
            if(newPass.equals(confirmNewPass)){
                isTrue = true;
            }else{
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_edit_failure),
                        getResString(R.string.prompt_edit_pass_diff),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                isTrue = false;
            }
        }else{
            promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_edit_failure),
                    getResString(R.string.notice_password),
                    getResString(R.string.confirm), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    promptDialog.dismiss();
                }
            });
            promptDialog.show();
            isTrue = false;
        }
        return isTrue;
    }
}
