package cc.bruce.seagull.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.model.Category;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.ExampleUtil;
import cc.bruce.seagull.util.PackageUtil;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.TopBar;
import cn.jpush.android.api.JPushInterface;

/**
 * @author: Created by Lemon on 2015年6月28日 下午9:04:40
 * @Description: 首页界面
 */
public class MainActivity extends BaseActivity {

    @ViewInject(id = R.id.topBar)private TopBar topBar;
    @ViewInject(id = R.id.llMainMenu)private LinearLayout llMainMenu;
    /** 搜索框*/
    @ViewInject(id = R.id.rlMainSearch)private RelativeLayout rlMainSearch;
    private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;
    /** 数据源*/
    private List<Category> categoryList;
    /** 完整的下载路径*/
    private String versionUrl;
    /** 获取最新版本成功*/
    private static final int GET_VERSION_SUCCESS = 200;
    /** 获取下载路径成功*/
    private static final int GET_VERSION_URL_SUCCESS = 300;

    private boolean isQuit = false;
    private Timer timer = new Timer();
    /** 图片宽度,根据屏幕宽度计算*/
    private int width;

    public static boolean isForeground = false;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        registerMessageReceiver();  // used for receive msg
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        int screenWidth = metric.widthPixels; // 屏幕宽度（像素）
        width = screenWidth / 3;
        topBar.settitleViewText(getResources().getString(R.string.title_main));
        topBar.setButtonImage(TopBar.RIGHT_BUTTON, R.drawable.bg_btn_setting_selector);
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.LEFT_IMGVIEW);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setRightButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                //跳转至设置中心
                startActivity(new Intent(mContext, SettingActivity.class));
            }
        });
        rlMainSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SearchActivity.class));
            }
        });
    }

    @Override
    protected void initData() {

        getDeviceInfo(this);
        System.out.println("getDeviceInfo:" + getDeviceInfo(this));

        finalHttp = new FinalHttp();
        loadingDialog = new MyLoadingDialog(mContext);
        AjaxParams ajaxParams = new AjaxParams();
        ajaxParams.put("cat", "book");
        Upload(ajaxParams, API.server + API.CATEGORY, SUCCESS);

        Upload(new AjaxParams(), API.server + API.APIVERSION, GET_VERSION_SUCCESS);


        // 初始化 JPush。如果已经初始化，但没有登录成功，则执行重新登录。
        JPushInterface.init(getApplicationContext());
    }

    /**
     * 用于测试友盟，生成的设备代码
     */
    public static String getDeviceInfo(Context context) {
        try {
            org.json.JSONObject json = new org.json.JSONObject();
            android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            String device_id = tm.getDeviceId();
            android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            String mac = wifi.getConnectionInfo().getMacAddress();
            json.put("mac", mac);
            if (TextUtils.isEmpty(device_id)) {
                device_id = mac;
            }
            if (TextUtils.isEmpty(device_id)) {
                device_id = android.provider.Settings.Secure.getString(
                        context.getContentResolver(),
                        android.provider.Settings.Secure.ANDROID_ID);
            }
            json.put("device_id", device_id);
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        JSONArray data = object.optJSONArray("data");
                        if(data != null && data.length() > 0) {
                            categoryList = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                Category category = new Category().parse(data.optJSONObject(i));
                                categoryList.add(category);
                            }
                            initMenuLayout(categoryList);
                        }
                    }
                    break;
                case GET_VERSION_SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        if(object.optJSONObject("data") != null && object.optJSONObject("data").length() > 0) {
                            if(!StringUtils.isBlank(object.optJSONObject("data").optString("ver"))
                                    && !object.optJSONObject("data").optString("ver").equals("null")) {
                                String newVersion = object.optJSONObject("data").optString("ver");
                                String oldVersion = PackageUtil.getAppVersionName(mContext);
                                if(Double.parseDouble(newVersion) > Double.parseDouble(oldVersion)) {
                                    Log.e(TAG, "newVersion->" + newVersion + "| oldVersion->" + oldVersion);
                                    AjaxParams ajaxParam = new AjaxParams();
                                    ajaxParam.put("version", newVersion);
                                    Upload(ajaxParam, API.server + API.APIAPP, GET_VERSION_URL_SUCCESS);
                                }//TODO 版本检测时，需要在else中提示用户是最新版本
                            }
                        }
                    }
                    break;
                case GET_VERSION_URL_SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        if(!StringUtils.isBlank(object.optString("download_url"))) {
                            versionUrl = API.server + object.optString("download_url");
                            Log.e(TAG, "下载APP路径->" + versionUrl);
                            promptDialog = new PromptDialog(mContext, "提示", "发现新版本", "马上更新", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //下载
                                    downloadApk(versionUrl);
                                    promptDialog.dismiss();
                                }
                            });
                            promptDialog.show();
                        }
                    }
                    break;
            }
        }
    };

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                loadingDialog.dismiss();
                if(API.equals(cc.bruce.seagull.model.API.server + cc.bruce.seagull.model.API.CATEGORY)) {
                    promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                            getResString(R.string.prompt_not_work_),
                            "重新发送请求", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AjaxParams ajaxParams = new AjaxParams();
                            ajaxParams.put("cat", "book");
                            Upload(ajaxParams, cc.bruce.seagull.model.API.server + cc.bruce.seagull.model.API.CATEGORY, SUCCESS);
                            promptDialog.dismiss();
                        }
                    });
                    promptDialog.show();
                    Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
                }

            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }

    /***
     * 加载菜单控件
     *
     * @param categoryList
     */
    private void initMenuLayout(List<Category> categoryList) {
        if (categoryList != null && categoryList.size() > 0) {
            for (int i = 0; i < categoryList.size(); i++) {
                if (i % 3 == 0) {
                    View v = getLayoutInflater().inflate(R.layout.layout_main_item, null);
                    RelativeLayout rl1 = (RelativeLayout) v.findViewById(R.id.rlMainMenu1);
                    RelativeLayout rl2 = (RelativeLayout) v.findViewById(R.id.rlMainMenu2);
                    RelativeLayout rl3 = (RelativeLayout) v.findViewById(R.id.rlMainMenu3);
                    ImageView ivMainMenu1 = (ImageView) v.findViewById(R.id.ivMainMenu1);
                    ImageView ivMainMenu2 = (ImageView) v.findViewById(R.id.ivMainMenu2);
                    ImageView ivMainMenu3 = (ImageView) v.findViewById(R.id.ivMainMenu3);
                    TextView tv1 = (TextView) v.findViewById(R.id.tvMainMenu1);
                    TextView tv2 = (TextView) v.findViewById(R.id.tvMainMenu2);
                    TextView tv3 = (TextView) v.findViewById(R.id.tvMainMenu3);

                    rl1.setTag(categoryList.get(i));
                    ivMainMenu1.setBackgroundResource(R.drawable.bg_btn_main_selector);
                    ivMainMenu1.setImageResource(GetDrawable(i));
                    tv1.setText(categoryList.get(i).category_name);
                    rl1.setOnClickListener(new MenuOnClick());
                    if (i + 1 < categoryList.size()) {
                        rl2.setTag(categoryList.get(i + 1));
                        ivMainMenu2.setBackgroundResource(R.drawable.bg_btn_main_selector);
                        ivMainMenu2.setImageResource(GetDrawable(i + 1));
                        tv2.setText(categoryList.get(i + 1).category_name);
                        rl2.setOnClickListener(new MenuOnClick());
                    }
                    if (i + 2 < categoryList.size()) {
                        rl3.setTag(categoryList.get(i + 2));
                        ivMainMenu3.setBackgroundResource(R.drawable.bg_btn_main_selector);
                        ivMainMenu3.setImageResource(GetDrawable(i + 2));
                        tv3.setText(categoryList.get(i + 2).category_name);
                        rl3.setOnClickListener(new MenuOnClick());
                    }
                    llMainMenu.addView(v);
                }
            }
        }
    }

    private int GetDrawable(int i) {
        int ResourceId = R.mipmap.module1;
        switch (i){
            case 0:
                ResourceId = R.mipmap.module1;
                break;
            case 1:
                ResourceId = R.mipmap.module2;
                break;
            case 2:
                ResourceId = R.mipmap.module3;
                break;
            case 3:
                ResourceId = R.mipmap.module4;
                break;
            case 4:
                ResourceId = R.mipmap.module5;
                break;
            case 5:
                ResourceId = R.mipmap.module6;
                break;
            case 6:
                ResourceId = R.mipmap.module7;
                break;
            case 7:
                ResourceId = R.mipmap.module8;
                break;
            case 8:
                ResourceId = R.mipmap.module9;
                break;
        }
        return ResourceId;
    }

    class MenuOnClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Log.e(TAG, "categoryName->" + ((Category)v.getTag()).category_name);
            Intent intent = new Intent(mContext, CategoryListActivity.class);
            intent.putExtra(Constants.INTENT_CATEGORY, (Category)v.getTag());
            startActivity(intent);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return Key_Down(keyCode, event);
    }

    public boolean Key_Down(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isQuit == false) {
                isQuit = true;
                Toast.makeText(getBaseContext(), "再按一次返回键退出程序",
                        Toast.LENGTH_SHORT).show();
                TimerTask task = null;
                task = new TimerTask() {
                    @Override
                    public void run() {
                        isQuit = false;
                    }
                };
                timer.schedule(task, 2000);
            } else {
                finish();
                System.exit(0);
            }
        }
        return isQuit;
    }


    private void downloadApk(String url){
        final ProgressDialog downloadDialog = new ProgressDialog(mContext, R.style.LightDialog);
        downloadDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        downloadDialog.setTitle("正在下载");
        downloadDialog.setIndeterminate(false);
        downloadDialog.setCancelable(false);
        downloadDialog.setCanceledOnTouchOutside(false);
        downloadDialog.show();
        finalHttp.download(url, Constants.APK_TARGET, new AjaxCallBack<File>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                System.out.println("errorNo:" + errorNo + ",strMsg:" + strMsg);
                downloadDialog.dismiss();
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_request_failure),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
                int progress;
                if (current != count && current != 0) {
                    progress = (int) (current / (float) count * 100);
                } else {
                    progress = 100;
                }
                downloadDialog.setProgress(progress);
                Log.e(TAG, "download:" + progress + "%");
            }

            @Override
            public void onStart() {
                super.onStart();
                Log.e(TAG, "start download");
            }

            @Override
            public void onSuccess(File t) {
                super.onSuccess(t);
                Log.e(TAG, "download success");
                PackageUtil.installNormal(mContext, Constants.APK_TARGET);
                downloadDialog.dismiss();
            }
        });
    }


    //for receive customer msg from jpush server
    private MessageReceiver mMessageReceiver;
    public static final String MESSAGE_RECEIVED_ACTION = "com.example.jpushdemo.MESSAGE_RECEIVED_ACTION";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";

    public void registerMessageReceiver() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(MESSAGE_RECEIVED_ACTION);
        registerReceiver(mMessageReceiver, filter);
    }

    public class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                String messge = intent.getStringExtra(KEY_MESSAGE);
                String extras = intent.getStringExtra(KEY_EXTRAS);
                StringBuilder showMsg = new StringBuilder();
                showMsg.append(KEY_MESSAGE + " : " + messge + "\n");
                if (!ExampleUtil.isEmpty(extras)) {
                    showMsg.append(KEY_EXTRAS + " : " + extras + "\n");
                }
                setCostomMsg(showMsg.toString());
            }
        }
    }

    private void setCostomMsg(String msg){
        prompt("setCostomMsg->" + msg);
//        if (null != msgText) {
//            msgText.setText(msg);
//            msgText.setVisibility(android.view.View.VISIBLE);
//        }
    }

    @Override
    protected void onResume() {
        isForeground = true;
        super.onResume();
    }


    @Override
    protected void onPause() {
        isForeground = false;
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }
}
