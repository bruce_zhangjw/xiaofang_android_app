package cc.bruce.seagull.activity;

import android.graphics.Bitmap;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import cc.bruce.seagull.R;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/18.
 * Desc:交流区
 */
public class BBSActivity extends BaseActivity {

    @ViewInject(id = R.id.topBar)private TopBar topBar;
    @ViewInject(id = R.id.wbBBS)private WebView wbBBS;
//    @ViewInject(id = R.id.progressBar)private ProgressBar progressBar;
    private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;

    @Override
    protected int getContentView() {
        return R.layout.activity_bbs;
    }

    @Override
    protected void initView() {
        topBar.settitleViewText("交流区");
        topBar.setHiddenButton(TopBar.RIGHT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        finalHttp = new FinalHttp();
        loadingDialog = new MyLoadingDialog(mContext);
    }

    @Override
    protected void initData() {
        wbBBS.getSettings().setJavaScriptEnabled(true);
        wbBBS.getSettings().setDatabaseEnabled(true);
        wbBBS.getSettings().setDomStorageEnabled(true);
        wbBBS.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        wbBBS.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);

            }
        });

        wbBBS.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
            }

            @Override
            public void onHideCustomView() {
                super.onHideCustomView();
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
        wbBBS.loadUrl("http://221.228.72.35:3334/www/index.php?m=forum&f=index&t=mobile&account="
                + PreferencesUtils.getString(mContext, Constants.KEY_ACCOUNT)
                + "&password="
                + PreferencesUtils.getString(mContext, Constants.KEY_PASS));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && wbBBS.canGoBack()) {
            wbBBS.goBack();// 返回前一个页面
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
