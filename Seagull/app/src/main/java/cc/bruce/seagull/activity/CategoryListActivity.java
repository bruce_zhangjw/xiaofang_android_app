package cc.bruce.seagull.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cc.bruce.seagull.R;
import cc.bruce.seagull.adapter.SecondListAdapter;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.model.Category;
import cc.bruce.seagull.model.Content;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.RefreshListView;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/4.
 * Desc:二级页面，分类列表展示
 */
public class CategoryListActivity extends BaseActivity {

    /** 列表*/
    @ViewInject(id = R.id.listCategory)private RefreshListView listCategory;
    /** 头部*/
    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 搜索layout*/
    @ViewInject(id = R.id.rlCategorySearch)private RelativeLayout rlCategorySearch;
    /** 数据源*/
    private List<Content> contentList;
    /** 适配器*/
    private SecondListAdapter secondListAdapter;
    private FinalHttp finalHttp;
    /** 提示对话框狂*/
    private PromptDialog promptDialog;
    /** 加载对话框狂*/
    private MyLoadingDialog loadingDialog;
    /** 分类实体*/
    private Category category;
    /** 是否到最后一页*/
    boolean doNotOver = true;
    /** 是否已经提醒过一遍*/
    boolean isTip = false;
    private int page = Constants.START;
    private int pageSize = Constants.END;
    private int count = Constants.COUNT;

    @Override
    protected int getContentView() {
        return R.layout.activity_category_list;
    }

    @Override
    protected void initView() {
        category = (Category) getIntent().getExtras().getSerializable(Constants.INTENT_CATEGORY);
        topBar.settitleViewText(category.category_name);
        topBar.setHiddenButton(TopBar.RIGHT_BUTTON);
        topBar.setHiddenButton(TopBar.RIGHT_IMGVIEW);
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setHiddenButton(TopBar.SECOND_RIGHT_IMGVIEW);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rlCategorySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SearchActivity.class);
                intent.putExtra(Constants.INTENT_CATE_NAME, category.category_name);
                startActivity(intent);
            }
        });
    }

    /** 请求封装参数*/
    private void AjaxParamsToUpload(int start, int end) {
        AjaxParams ajaxParams = new AjaxParams();
        ajaxParams.put("catid", category.category_id);
        ajaxParams.put("start", start + "");
        ajaxParams.put("end", end + "");
        Upload(ajaxParams, API.server + API.APICATLIST, SUCCESS);
    }

    @Override
    protected void initData() {
        finalHttp = new FinalHttp();
        loadingDialog = new MyLoadingDialog(mContext);

        AjaxParamsToUpload(page, pageSize);

        secondListAdapter = new SecondListAdapter(CategoryListActivity.this, R.layout.list_item_second);
        listCategory.setAdapter(secondListAdapter);

        listCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra(Constants.INTENT_DETAILID, secondListAdapter.getDataList().get(position - 1).detailid);
                intent.putExtra(Constants.INTENT_CATEGORY, category.category_name);
                startActivity(intent);
            }
        });

        listCategory.setOnLoadMoreListenter(new RefreshListView.OnLoadMoreListener() {

            public void onLoadMore() {
                Log.e(TAG, "setOnLoadMoreListenter");
                if (doNotOver) {
                    pageSize = pageSize + count;
                    AjaxParamsToUpload(page, pageSize);
                } else {
                    listCategory.onLoadMoreComplete();
                    if (!isTip) {
                        isTip = true;
                        prompt("已经到底了");
                    }
                }
            }
        });
        listCategory.setOnRefreshListener(new RefreshListView.OnRefreshListener() {

            public void onRefresh() {
                Log.e(TAG, "setOnRefreshListener");
                pageSize = Constants.END;
                AjaxParamsToUpload(page, pageSize);
            }
        });
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    loadingDialog.dismiss();
                    listCategory.onRefreshComplete();
                    listCategory.onLoadMoreComplete();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        JSONArray data = object.optJSONArray("data");
                        if(!StringUtils.isBlank(object.optString("listnum"))) {
                            int totalCount =  Integer.parseInt(object.optString("listnum"));
                            if(pageSize >= totalCount) {
                                //已经请求到最后的数据
                                doNotOver = false;
                            } else {
                                //还有一下也
                                doNotOver = true;
                            }
                        }
                        if(data != null && data.length() > 0) {
                            contentList = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                Content content = new Content().parse(data.optJSONObject(i));
                                contentList.add(content);
                            }
                            secondListAdapter.getDataList().clear();
                            secondListAdapter.getDataList().addAll(contentList);
                            secondListAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
            }
        }
    };

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_request_failure),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                    }
                });
                promptDialog.show();
                listCategory.onRefreshComplete();
                listCategory.onLoadMoreComplete();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                    listCategory.onRefreshComplete();
                    listCategory.onLoadMoreComplete();
                }
            }
        });
    }
}
