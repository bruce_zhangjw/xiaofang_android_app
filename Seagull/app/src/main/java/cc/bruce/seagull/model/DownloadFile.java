package cc.bruce.seagull.model;

import org.json.JSONObject;

/**
 * Created by Lemon on 2015/8/8.
 * Desc:详情内文件下载实体类
 */
public class DownloadFile extends BaseModel<DownloadFile> {

    /** 文件名称*/
    public String title;
    /** 下载路径*/
    public String url;

    @Override
    public DownloadFile parse(JSONObject jsonObject) {
        if(jsonObject != null && jsonObject.length() >0) {
            title = jsonObject.optString("title");
            url = jsonObject.optString("url");
            return this;
        }
        return null;
    }
}
