package cc.bruce.seagull.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import cc.bruce.seagull.R;
import cc.bruce.seagull.model.Content;
import cc.bruce.seagull.model.DownloadFile;

/**
 * Created by Lemon on 2015/8/8.
 * Desc:下载文件列表适配器
 */
public class DownloadFileAdapter extends AbsAdapter<DownloadFile>{

    public DownloadFileAdapter(Activity context, int layout) {
        super(context, layout);
    }

    @Override
    public ViewHolder<DownloadFile> getHolder() {
        return new DownloadFileViewHolder();
    }


    private class DownloadFileViewHolder implements ViewHolder<DownloadFile> {

        /** 标题*/
        private TextView tvDownFileTitle;

        @Override
        public void initViews(View v, int position) {
            tvDownFileTitle = (TextView) v.findViewById(R.id.tvDownFileTitle);
        }

        @Override
        public void updateData(DownloadFile downloadFile, int position) {
            tvDownFileTitle.setText(downloadFile.title);
        }

        @Override
        public void doOthers(DownloadFile downloadFile, int position) {

        }
    }
}
