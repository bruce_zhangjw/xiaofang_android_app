package cc.bruce.seagull.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageStats;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.UserHandle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.DisplayMetrics;

import java.io.File;
import java.lang.reflect.Method;

/**
 * @ClassName: DeviceUtil
 * @Description: TODO
 * @author caojiabing(曹家兵) caojiabing@foxmail.com
 * @date 2014-11-14 下午1:25:03
 */
public class AppInfoUtil {
	/**
	 * 
	 * @Title: getVersionName
	 * @Description: 获取当前应用的版本号
	 * @param @param context
	 * @param @return
	 * @return String
	 * @throws
	 */
	public static String getVersionName(Context context) {
		// 获取packagemanager的实例
		try {
			PackageManager packageManager = context.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(context.getPackageName(),
					0);
			String version = packInfo.versionName;
			return version;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	public static int getVersionCode(Context context) {
		// 获取packagemanager的实例
		try {
			PackageManager packageManager = context.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo;
			packInfo = packageManager.getPackageInfo(context.getPackageName(),
					0);
			int version = packInfo.versionCode;
			return version;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 100000;
		}
	}

    private String pkgName;

	/**
	 * 
	 * @Title: getPkgSize 
	 * @Description: TODO 
	 * @param @param context
	 * @param @param handler
	 * @param @param waht    
	 * @return void     
	 * @throws
	 */
	@SuppressLint("NewApi") 
	public static void getPkgSize(final Context context, final Handler handler,
			final int waht) {
            int sysVersion = Build.VERSION.SDK_INT;
            if (context.getPackageName() != null) {// packageName
                PackageManager pm = context.getPackageManager();
                try {
                    Class<?> clz = pm.getClass();
                    if (sysVersion > 16) {
                        Method myUserId = UserHandle.class.getDeclaredMethod("myUserId");//ignore check this when u set ur min SDK < 17
                        int userID = (Integer) myUserId.invoke(pm);
                        Method getPackageSizeInfo = clz.getDeclaredMethod("getPackageSizeInfo", String.class, int.class,IPackageStatsObserver.class);//remember add int.class into the params
                        getPackageSizeInfo.invoke(pm, context.getPackageName(), userID, new SizeObserver(context, handler, waht));
                    } else {
                        Method getPackageSizeInfo = clz.getDeclaredMethod("getPackageSizeInfo", String.class,IPackageStatsObserver.class);
                        getPackageSizeInfo.invoke(pm, context.getPackageName(), new SizeObserver(context, handler, waht));
                    }
//                    // getPackageSizeInfo是PackageManager中的一个private方法，所以需要通过反射的机制来调用
//			Method method = PackageManager.class.getMethod(
//					"getPackageSizeInfo", new Class[] { String.class,
//							IPackageStatsObserver.class });
//			// 调用 getPackageSizeInfo 方法，需要两个参数：1、需要检测的应用包名；2、回调
//			method.invoke(context.getPackageManager(),
//					new Object[] { context.getPackageName(),
//							new IPackageStatsObserver.Stub() {
//								@Override
//								public void onGetStatsCompleted(
//										PackageStats pStats, boolean succeeded)
//										throws RemoteException {
//									// TODO Auto-generated method stub
//									long size = 0;
//
//									size += pStats.cacheSize;
//									size += pStats.externalCacheSize;
//									size += pStats.externalDataSize;
//									size += pStats.dataSize;
//
//									Message message = Message.obtain();
//									message.what = waht;
//
//									message.obj = Formatter.formatFileSize(
//											context, size);
//
//									handler.sendMessage(message);
//
//								}
//							} });
                } catch (Exception e) {
                    Message message = Message.obtain();
                    message.what = waht;
                    message.obj = "0.0" + " KB";
                    handler.sendMessage(message);
                    e.printStackTrace();
                }
            }
    }

    static class SizeObserver  extends IPackageStatsObserver.Stub {

        private Context context;
        private Handler handler;
        private int what;

        public SizeObserver(Context context, Handler handler, int what) {
            this.context = context;
            this.handler = handler;
            this.what = what;
        }

        @Override
        public void onGetStatsCompleted(PackageStats pStats, boolean succeeded) throws RemoteException {
            long size = 0;
            size += pStats.cacheSize;
            size += pStats.externalCacheSize;
            size += pStats.externalDataSize;
            size += pStats.dataSize;
            Message message = Message.obtain();
            message.what = this.what;
            message.obj = Formatter.formatFileSize(context, size);
            handler.sendMessage(message);
        }
    }

	/**
	 * 
	 * @Title: cleanApplicationData 
	 * @Description: TODO 
	 * @param @param context
	 * @param @param handler
	 * @param @param what    
	 * @return void     
	 * @throws
	 */
	@SuppressLint("SdCardPath") 
	public static void cleanApplicationData(final Context context,
			final Handler handler,final int what) {
		new Thread() {
			@Override
			public void run() {
				try {
					// 清除本应用内部缓存(/data/data/com.xxx.xxx/cache)
					deleteFilesByDirectory(context.getCacheDir());
					// 清除本应用所有数据库(/data/data/com.xxx.xxx/databases)
					deleteFilesByDirectory(new File("/data/data/" + context.getPackageName() + "/databases"));

					// 清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs)
					deleteFilesByDirectory(new File("/data/data/"
							+ context.getPackageName() + "/shared_prefs"));

					// 清除/data/data/com.xxx.xxx/files下的内容
					deleteFilesByDirectory(context.getFilesDir());
					
					// 清除外部cache下的内容(/mnt/sdcard/android/data/com.xxx.xxx/cache)
					if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
						deleteFilesByDirectory(context.getExternalCacheDir());
					}
					
					handler.sendEmptyMessage(what);
				} catch (Exception e) {
					handler.sendEmptyMessage(what);
					e.printStackTrace();
				}
			}

		}.start();
	}

	/**
	 * 
	 * @Title: deleteFilesByDirectory 
	 * @Description: TODO 
	 * @param @param directory    
	 * @return void     
	 * @throws
	 */
	private static void deleteFilesByDirectory(File directory) {
		if (directory != null && directory.exists() && directory.isDirectory()) {
			for (File item : directory.listFiles()) {
				item.delete();
			}
		}
	}
	
	/**
	 * 
	 * @Title: getDeviceInfo 
	 * @Description: TODO 
	 * @param @param context
	 * @param @return    
	 * @return String     
	 * @throws
	 */
	public static String getDeviceInfo(Context context) {
	        try{
	          org.json.JSONObject json = new org.json.JSONObject();
	          android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
	              .getSystemService(Context.TELEPHONY_SERVICE);

	          String device_id = tm.getDeviceId();

	          android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) context.getSystemService(Context.WIFI_SERVICE);

	          String mac = wifi.getConnectionInfo().getMacAddress();
	          json.put("mac", mac);

	          if( TextUtils.isEmpty(device_id) ){
	            device_id = mac;
	          }

	          if( TextUtils.isEmpty(device_id) ){
	            device_id = android.provider.Settings.Secure.getString(context.getContentResolver(),android.provider.Settings.Secure.ANDROID_ID);
	          }

	          json.put("device_id", device_id);

	          return json.toString();
	        }catch(Exception e){
	          e.printStackTrace();
	        }
	      return null;
	    }

	public static float getScreenDensity(Context context) {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		float density = dm.density;
		return density;
	}

}