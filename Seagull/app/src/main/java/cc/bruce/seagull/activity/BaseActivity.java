package cc.bruce.seagull.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONException;
import org.json.JSONObject;

import cc.bruce.seagull.R;
import cc.bruce.seagull.util.ActivityStackManager;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.util.StringUtils;
/**
 *
 * @author: Created by Lemon on 2014年6月21日 下午3:24:14
 * @Description: 所有Activity的基类
 */
public abstract class BaseActivity extends FinalActivity {

	protected Context mContext;
	/** 请求获取成功标石*/
	protected static final int SUCCESS = 100;
	/** 请求获取失败标石*/
	protected static final int FAILURE = 200;
	/** 打印标签*/
	protected static String TAG = "TAG";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityStackManager.getActivityManager().push(this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContext = this;
		TAG = mContext.getClass().getSimpleName();
		setContentView(getContentView());
		initView();
		initData();
	}

	/***
	 * TOAST
	 * @param msg
	 */
	protected void prompt(String msg) {
		if(mContext != null) {
			Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
		}
	}
	/***
	 * TOAST
	 * @param StringId
	 */
	protected void prompt(int StringId) {
        if(mContext != null) {
            Toast.makeText(mContext, StringId, Toast.LENGTH_SHORT).show();
        }
    }

    protected String getResString(int StringId) {
        return mContext.getResources().getString(StringId);
    }

    /***
     * 公共解析
     * @param t
     * @return 完整json
     */
    protected JSONObject BaseJsonData(String t) {
        JSONObject object = null;
        try {
            object = new JSONObject(t);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

	/** 删除提示对话框 */
	public void DeletePromoptdialog(OnClickListener onClickListener) {
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage(getResources().getString(R.string.dialog_delete));
		builder.setTitle(getResources().getString(R.string.notice));
		builder.setPositiveButton(getResources().getString(R.string.confirm), onClickListener);
		builder.setNegativeButton(getResources().getString(R.string.cancel),
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.create().show();
	}

	@Override
	public final void startActivity(Intent intent) {
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		super.startActivity(intent);
	}

	/** 本类存储gere至本地属性*/
	protected void savePreferences(String uid, String account, String username){
		PreferencesUtils.putString(this, Constants.KEY_UID, uid);
		PreferencesUtils.putString(this, Constants.KEY_ACCOUNT, account);
		PreferencesUtils.putString(this, Constants.KEY_USERNAME, username);
	}

	/***
	 * 修改部分文本字体颜色
	 * @param text :需要修改的文本整体字段
	 * @param color :色值
	 * @param startIndex :需要修改的文字开始下标
	 * @param endIndex :需要修改的文字结束下标
	 * @param isForeground :true->设置文本前景颜色  | false->设置背景文本背景颜色
	 * @return
	 */
	protected SpannableStringBuilder ModifyTextpartColor(String text, int color, int startIndex, int endIndex, Boolean isForeground) {
		SpannableStringBuilder builder = new SpannableStringBuilder(text);
		if(isForeground) {
			ForegroundColorSpan span= new ForegroundColorSpan(color);
			builder.setSpan(span, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}else{
			BackgroundColorSpan span= new BackgroundColorSpan(color);
			builder.setSpan(span, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		return builder;
	}

	/**
	 * 时间计数器，最多只能到99小时，如需要更大小时数需要改改方法
	 * @param time
	 * @return
	 */
	protected String showTimeCount(long time) {
		System.out.println("time=" + time);
		if (time >= 360000000) {
			return "00:00:00";
		}
		String timeCount = "";
		long hourc = time / 3600000;
		String hour = "0" + hourc;
		System.out.println("hour=" + hour);
		hour = hour.substring(hour.length() - 2, hour.length());
		System.out.println("hour2=" + hour);

		long minuec = (time - hourc * 3600000) / (60000);
		String minue = "0" + minuec;
		System.out.println("minue=" + minue);
		minue = minue.substring(minue.length() - 2, minue.length());
		System.out.println("minue2=" + minue);

		long secc = (time - hourc * 3600000 - minuec * 60000) / 1000;
		String sec = "0" + secc;
		System.out.println("sec=" + sec);
		sec = sec.substring(sec.length() - 2, sec.length());
		System.out.println("sec2=" + sec);
		timeCount = hour + ":" + minue + ":" + sec;
		System.out.println("timeCount=" + timeCount);
		return timeCount;
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	protected abstract int getContentView();
	/**
	 * 初始化UI控件
	 */
	protected abstract void initView();
	/**
	 * 初始化数据
	 */
	protected abstract void initData();
}



