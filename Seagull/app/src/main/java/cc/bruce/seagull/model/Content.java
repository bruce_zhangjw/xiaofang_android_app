package cc.bruce.seagull.model;

import org.json.JSONObject;
/**
 * Created by Lemon on 2015/7/7.
 * Desc:文章内容实体类(二级页面的列表以及详情内容)
 */
public class Content extends BaseModel<Content> {

    /** 单页内容ID*/
    public String detailid;
    /** 标题(二级页面的标题)*/
    public String title;
    /** 标题(详情的标题)*/
    public String name;
    /** 发问机关*/
    public String port;
    /** 文号*/
    public String proof;
    /** 摘要*/
    public String summary;
    /** 发文时间*/
    public String pubtime;
    /** 正文内容*/
    public String content;

    @Override
    public Content parse(JSONObject jsonObject) {
        if(jsonObject != null && jsonObject.length() > 0) {
            detailid = jsonObject.optString("detailid");
            title = jsonObject.optString("title");
            name = jsonObject.optString("name");
            port = jsonObject.optString("port");
            proof = jsonObject.optString("proof");
            summary = jsonObject.optString("summary");
            pubtime = jsonObject.optString("pubtime");
            content = jsonObject.optString("content");
            return  this;
        }
        return null;
    }
}
