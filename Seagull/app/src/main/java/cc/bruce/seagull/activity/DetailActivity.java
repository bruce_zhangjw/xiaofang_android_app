package cc.bruce.seagull.activity;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;
import org.json.JSONObject;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import cc.bruce.seagull.R;
import cc.bruce.seagull.adapter.DownloadFileAdapter;
import cc.bruce.seagull.model.API;
import cc.bruce.seagull.model.Content;
import cc.bruce.seagull.model.DownloadFile;
import cc.bruce.seagull.util.AppInfoUtil;
import cc.bruce.seagull.util.Constants;
import cc.bruce.seagull.util.PackageUtil;
import cc.bruce.seagull.util.PreferencesUtils;
import cc.bruce.seagull.util.StringUtils;
import cc.bruce.seagull.util.Utility;
import cc.bruce.seagull.view.CollectResultDialog;
import cc.bruce.seagull.view.DetailSearchDialog;
import cc.bruce.seagull.view.DetailFeedbackDialog;
import cc.bruce.seagull.view.DetailMenuDialog;
import cc.bruce.seagull.view.MyLoadingDialog;
import cc.bruce.seagull.view.PromptDialog;
import cc.bruce.seagull.view.TopBar;

/**
 * Created by Lemon on 2015/7/7.
 * Desc:详情页面
 */
public class DetailActivity extends BaseActivity {

    /** 头部*/
    @ViewInject(id = R.id.topBar)private TopBar topBar;
    /** 标题*/
    @ViewInject(id = R.id.tvDetailTitle)private TextView tvDetailTitle;
    /** 发文机关*/
    @ViewInject(id = R.id.tvDetailPort)private TextView tvDetailPort;
    /** 文号*/
    @ViewInject(id = R.id.tvDetailProof)private TextView tvDetailProof;
    /** 发文时间*/
    @ViewInject(id = R.id.tvDetailPubTime)private TextView tvDetailPubTime;
    /** 摘要*/
    @ViewInject(id = R.id.tvDetailSummary)private TextView tvDetailSummary;
    /** 内容*/
    @ViewInject(id = R.id.webDetailContent)private WebView webDetailContent;
    @ViewInject(id = R.id.scrollViewDetail)private ScrollView scrollView;
    /** 需要下载的文件列表*/
    @ViewInject(id = R.id.listFile)private ListView listFile;
    /** 获取详情数据成功标石*/
    private static final int GET_DETAIL_SUCCESS = 100;
    /** 收藏成功标石*/
    private static final int COLLECT_SUCCESS = 200;
    /** 纠错成功标石*/
    private static final int CORRECTION_SUCCESS = 300;
    private FinalHttp finalHttp;
    /** 提示对话框*/
    private PromptDialog promptDialog;
    /** 加载对话框*/
    private MyLoadingDialog loadingDialog;
    /** 详情实体类*/
    private Content content;
    /** 当前文章是否收藏*/
    private boolean isCollect = false;
    /** 当前文章内容ID*/
    private String detailId;
    /** 菜单对话框*/
    private DetailMenuDialog detailMenuDialog;
    /** 搜索对话框*/
    private DetailSearchDialog detailSearchDialog;
    /** 收藏后结果展示对话框*/
    private CollectResultDialog resultDialog = null;
    /** 纠错对话框*/
    private DetailFeedbackDialog feedbackDialog = null;
    MyJavascriptInterface myJavascriptInterface;
    /** 当前高亮的索引位置*/
    private int index = 1;
    /** 下载文件数据源*/
    private List<DownloadFile> downloadFileList;
    /** 下载文件列表适配器*/
    private DownloadFileAdapter fileAdapter;

    @Override
    protected int getContentView() {
        return R.layout.activity_second_detail;
    }

    @Override
    protected void initView() {
        String categoryName = getIntent().getExtras().getString(Constants.INTENT_CATEGORY);
        topBar.settitleViewText(categoryName);
        topBar.setHiddenButton(TopBar.RIGHT_BUTTON);
        topBar.setHiddenButton(TopBar.LEFT_BUTTON);
        topBar.setLeftButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        topBar.setRightButtonOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                //打开收藏纠错对话框
                detailMenuDialog = new DetailMenuDialog(mContext, isCollect, new onCollectClickListener(), new OnCorrectionListener());
                detailMenuDialog.show();
            }
        });
        topBar.setSecondRightImgviewOnClickListener(new TopBar.ButtonOnClick() {
            @Override
            public void onClick(View view) {
                //搜索
                detailSearchDialog.show();
            }
        });
        fileAdapter = new DownloadFileAdapter(DetailActivity.this, R.layout.list_item_download_file);
        listFile.setAdapter(fileAdapter);
        listFile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String title = fileAdapter.getDataList().get(position).title;
                String url = fileAdapter.getDataList().get(position).url;
                UploadFile(title, API.server_file + url);
            }
        });
    }

    @Override
    protected void initData() {
        finalHttp = new FinalHttp();
        loadingDialog = new MyLoadingDialog(mContext);
        detailSearchDialog = new DetailSearchDialog(mContext, new OnClearListener(),new OnPreviousListener(), new OnNextListener());
        detailId = getIntent().getExtras().getString(Constants.INTENT_DETAILID);
        AjaxParams ajaxParams = new AjaxParams();
        ajaxParams.put("detailid", detailId);
        Upload(ajaxParams, API.server + API.APIDETAIL, GET_DETAIL_SUCCESS);

        webDetailContent.getSettings().setJavaScriptEnabled(true);
        webDetailContent.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        webDetailContent.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);

            }
        });

        webDetailContent.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
            }

            @Override
            public void onHideCustomView() {
                super.onHideCustomView();
            }
        });

        myJavascriptInterface = new MyJavascriptInterface();
        webDetailContent.addJavascriptInterface(myJavascriptInterface, "searchCallback");
    }

//    class MyJavascriptInterface {
//        @JavascriptInterface
//        public void finish() {
//
//        }
//        @JavascriptInterface
//        public void getCount(int number) {
//            Message message = Message.obtain();
//            message.what = -1;
//            message.arg1 = number;
//            handler.sendMessage(message);
//        }
//    }

    class MyJavascriptInterface {
        @android.webkit.JavascriptInterface
        public void findListener(int currentSelectIndex,int searchCount,int y) {
            Message message = Message.obtain();
            message.what = -1;
            message.arg1 = y;
            message.arg2 = searchCount;
            message.obj = currentSelectIndex;
            handler.sendMessage(message);
        }
    }

    /** 显示数据*/
    private void showData() {
        tvDetailTitle.setText(content.title);
        tvDetailPort.setText("颁发单位：" + content.port);
        tvDetailProof.setText("文号：" + content.proof);
        tvDetailPubTime.setText("颁布日期：" + content.pubtime);
        tvDetailSummary.setText("摘要：" + content.summary);

//        webDetailContent.loadUrl("file:///android_asset/test.html");
        webDetailContent.loadDataWithBaseURL(null, content.content, "text/html", "UTF-8", null);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case GET_DETAIL_SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        String Collect= object.optString("iscollect");
                        JSONObject data = object.optJSONObject("data");

                        downloadFileList = new ArrayList<>();
                        if(object.optJSONArray("files") != null && object.optJSONArray("files").length() >0) {
                            for (int i = 0; i < object.optJSONArray("files").length(); i++) {
                                DownloadFile downloadFile = new DownloadFile().parse(object.optJSONArray("files").optJSONObject(i));
                                downloadFileList.add(downloadFile);
                            }
                        }
                        fileAdapter.getDataList().clear();
                        fileAdapter.getDataList().addAll(downloadFileList);
                        fileAdapter.notifyDataSetChanged();
                        Utility.setListViewHeightBasedOnChildren(listFile);

                        if(Collect.equals("false")) {
                            isCollect = false;
                        }
                        if(Collect.equals("true")) {
                            isCollect = true;
                        }
                        if(data != null && data.length() > 0) {
                            content = new Content().parse(data);
                            showData();
                        }
                    }
                    break;
                case COLLECT_SUCCESS:
                    //message：0已存在or不存在；1 操作成功
                    loadingDialog.dismiss();
                    detailMenuDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        String message = object.optString("msg");
                        if(message.equals("0")) {
                            prompt("操作失败，请重试");
                        }
                        if(message.equals("1")) {
                            if(isCollect) {
                                resultDialog = new CollectResultDialog(mContext, "已取消收藏");
                                isCollect = false;
                            }else {
                                resultDialog = new CollectResultDialog(mContext, "已成功收藏");
                                isCollect = true;
                            }
                            resultDialog.show();
                        }
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                resultDialog.dismiss();
                            }
                        }, 1500);
                    }
                    break;
                case CORRECTION_SUCCESS:
                    loadingDialog.dismiss();
                    if(msg.obj != null) {
                        JSONObject object = BaseJsonData((String) msg.obj);
                        String message = object.optString("msg");
                        if(message.equals("1")) {
                            prompt("纠错成功");
                            feedbackDialog.dismiss();
                        }
                    }
                    break;
//                case -1:
//                    totalCount = msg.arg1;
//                    detailSearchDialog.showSearchCount(totalCount);
//                    break;
                case -1:
                    detailSearchDialog.showSearchCount(msg.arg2, ((Integer) msg.obj)+1);
                    scrollView.smoothScrollTo(0, 0);
                    scrollView.smoothScrollTo(0, (int) (AppInfoUtil.getScreenDensity(DetailActivity.this)*msg.arg1));
                    break;
            }
        }
    };

    private int totalCount;

    /***
     * 公用请求
     * @param ajaxParams
     * @param API
     * @return
     */
    protected void Upload(AjaxParams ajaxParams, final String API, final int success) {
//        ajaxParams.put("timespan", "20160619");
//        ajaxParams.put("callback", "hoodbook");
        Log.e(TAG, Constants.REQUEST + API + "\n" + ajaxParams.toString());
        finalHttp.post(API, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                        getResString(R.string.prompt_request_failure),
                        getResString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        promptDialog.dismiss();
                        loadingDialog.dismiss();
                    }
                });
                promptDialog.show();
                Log.e(TAG, "errorNo:" + errorNo + ",strMsg:" + strMsg);
            }

            @Override
            public void onStart() {
                super.onStart();
                loadingDialog.show();
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                if (!StringUtils.isBlank(t)) {
                    Log.e(TAG, Constants.RESULT + API + "\n" + t.toString());
                    handler.sendMessage(handler.obtainMessage(success, t));
                } else {
                    prompt(getResources().getString(R.string.request_no_data));
                }
            }
        });
    }

    class onCollectClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //收藏操作
            AjaxParams ajaxParams = new AjaxParams();
            ajaxParams.put("uid", PreferencesUtils.getString(mContext, Constants.KEY_UID));
            ajaxParams.put("detailid", detailId);
            Upload(ajaxParams, API.server + API.APICOLLECT, COLLECT_SUCCESS);
        }
    }

    class onSendClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //发送纠错
            if(!StringUtils.isBlank(feedbackDialog.getFeedbackContent())){
                Log.e(TAG, "content->" + feedbackDialog.getFeedbackContent());
                AjaxParams ajaxParams = new AjaxParams();
                ajaxParams.put("uid", PreferencesUtils.getString(mContext, Constants.KEY_UID));
                ajaxParams.put("detailid", detailId);
                ajaxParams.put("content", feedbackDialog.getFeedbackContent());
                Upload(ajaxParams, API.server + API.APIREVIEW, CORRECTION_SUCCESS);
            }else {
                prompt("纠错不能为空");
            }
        }
    }

    class OnCorrectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //纠错操作
            detailMenuDialog.dismiss();
            feedbackDialog = new DetailFeedbackDialog(mContext, new onSendClickListener());
            feedbackDialog.show();
        }
    }

    class OnClearListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //清除操作
            detailSearchDialog.clearKeyWord();
//            webDetailContent.loadUrl("javascript:clearSelection()");
            webDetailContent.loadUrl("javascript:reset()");
        }
    }

    class OnPreviousListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //上一条
//            webDetailContent.loadUrl("javascript:prv()");
            webDetailContent.loadUrl("javascript:last()");
            if(index > 1) {
                index--;
                detailSearchDialog.changeIndex(index);
            }

        }
    }

    class OnNextListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //下一条
//            webDetailContent.loadUrl("javascript:aft()");
            webDetailContent.loadUrl("javascript:next()");
            if(index < totalCount){
                index++;
                detailSearchDialog.changeIndex(index);
            }
        }
    }

    /** 执行搜索关键字*/
    public void doSearchKeyWord(String key) {
        Log.e(TAG, "doSearchKeyWord");
//        webDetailContent.loadUrl("javascript:highlight('" + key + "')");
        webDetailContent.loadUrl("javascript:search('" + key + "')");
    }

    /** 执行清除*/
    public void doClear() {
        webDetailContent.loadUrl("javascript:clearSelection()");
    }

    /**
     * 下载文件
     * @param title 文件标题
     * @param url 文件路径
     */
    private void UploadFile(final String title, String url) {
        Log.e(TAG, "url->" + url);
        final ProgressDialog downloadDialog = new ProgressDialog(mContext, R.style.LightDialog);
        downloadDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        downloadDialog.setTitle("正在下载");
        downloadDialog.setIndeterminate(false);
        downloadDialog.setCancelable(false);
        downloadDialog.setCanceledOnTouchOutside(false);
        downloadDialog.show();
        finalHttp.download(url, Constants.TARGET_FILE + "/" + title, new AjaxCallBack<File>() {
            @Override
            public void onStart() {
                super.onStart();
                Log.e(TAG, "start download");
            }

            @Override
            public void onLoading(long count, long current) {
                super.onLoading(count, current);
                int progress;
                if (current != count && current != 0) {
                    progress = (int) (current / (float) count * 100);
                } else {
                    progress = 100;
                }
                downloadDialog.setProgress(progress);
                Log.e(TAG, "download:" + progress + "%");
            }

            @Override
            public void onSuccess(File file) {
                super.onSuccess(file);
                Log.e(TAG, "download success");
                downloadDialog.dismiss();
                if (file.exists()) {
                    Log.e(TAG, "打开->" + Constants.TARGET_FILE + "/" + title);
                    Intent intent = PackageUtil.openFile(Constants.TARGET_FILE + "/" + title);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                System.out.println("errorNo:" + errorNo + ",strMsg:" + strMsg);
                downloadDialog.dismiss();
                try {
                    throw t;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    promptDialog = new PromptDialog(mContext, getResString(R.string.prompt_request_failure),
                            "您刚下载过此文件，请稍后再试",
                            getResString(R.string.confirm), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            promptDialog.dismiss();
                        }
                    });
                    promptDialog.show();
                }
            }
        });
    }
}
