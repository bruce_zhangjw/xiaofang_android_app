package cc.bruce.seagull.model;

/**
 * Created by Lemon on 2015/7/4.
 * Desc:请求接口路径
 */
public class API {

    /** 服务器请求路镜头*/
//    public static final String server = "http://122.227.254.145:3334/www/..//system/api/";
//    public static final String server = "http://221.228.72.35:3334/www/..//system/api/";
    public static final String server = "http://www.superanchor.net//system/api/";
    /** 文件路镜头*/
//    public static final String server_file = "http://122.227.254.145:3334";
//    public static final String server_file = "http://221.228.72.35:3334";
    public static final String server_file = "http://www.superanchor.net";

    /** 登录uname=admin&pword=admin*/
    public static final String LOGIN = "apilogin.php";
    /** 类别cat=book*/
    public static final String CATEGORY = "apicategory.php";
    /** 分类列表end=10&start=0&catid=1*/
    public static final String APICATLIST = "apicatlist.php";
    /** 详情detailid=1*/
    public static final String APIDETAIL = "apidetail.php";
    /** 搜索port=太原消防科&cat=&keyword=&proof=*/
    public static final String APISEARCH = "apisearch.php";
    /** 收藏bookid=1&uid=1&method=0*/
    public static final String APICOLLECT = "apicollect.php";
    /** 纠错*/
    public static final String APIREVIEW = "apireview.php";
    /** 用户名*/
    public static final String APIUNAME = "apiuname.php";
    /** 修改密码*/
    public static final String APIPASWORD = "apipasword.php";
    /** 获取收藏列表*/
    public static final String APICOLLLIST = "apicolllist.php";
    /** 反馈*/
    public static final String APIVIEW = "apiview.php";
    /** 关于我们*/
    public static final String APIABOUT = "apiabout.php";
    /** 版本检测*/
    public static final String APIVERSION = "apiversion.php";
    /** 下载APP地址*/
    public static final String APIAPP = "apiapp.php";
}
