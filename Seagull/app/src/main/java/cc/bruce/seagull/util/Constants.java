package cc.bruce.seagull.util;

import android.os.Environment;

/**
 *
 * @author: Created by Lemon on 2015年7月2日 下午10:31:20
 * @Description: 常量类
 */
public class Constants {

    /** 请求标石*/
    public static final String REQUEST = "REQUEST";
    /** 请求返回标石*/
    public static final String RESULT = "RESULT";
    /** 列表获取的起始位置*/
    public static final int START = 0;
    /** 列表获取的结束位置*/
    public static final int END = 5;
    /** 每一页增加的条目数*/
    public static final int COUNT = 5;
    /** 版本更新APK保存路径*/
    public static final String APK_TARGET = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Seagull.apk";
    /** 文件下载存放路径*/
    public static final String TARGET_FILE = Environment.getExternalStorageDirectory().getAbsolutePath();

	/*********************** KEY标石  **************************/
    /** 用户ID*/
    public static final String KEY_UID = "KEY_UID";
    /** 登录密码：应在后台设置token是否过期标石，完成自动等率功能，暂时保存*/
    public static final String KEY_PASS = "KEY_PASS";
    /** 账户名（不可修改）*/
    public static final String KEY_ACCOUNT = "KEY_ACCOUNT";
    /** 用户名或昵称*/
    public static final String KEY_USERNAME = "KEY_USERNAME";

	/*********************** Intent传递的key字段  **************/
    /** 跳转到分类列表传递实体CATEGORY*/
	public static final String INTENT_CATEGORY = "INTENT_CATEGORY";
    /** 跳转到登录页面重新登录标石*/
	public static final String INTENT_RE_LOGIN = "INTENT_RE_LOGIN";
    /** 跳转到详情页面传递但内容ID标石*/
    public static final String INTENT_DETAILID = "INTENT_DETAILID";
    /** 跳转到高级搜索页面传递分类ID标石*/
    public static final String INTENT_CATE_NAME = "INTENT_CATE_ID";

}



